function showTimeline(filter, tag="#timeline", tipe="timeline"){
  var send_url = '/kinerja_harian_bawahan/timelineharian';
  if(tipe !== 'timeline'){
    send_url = send_url + '/tabel';
  }
  $.ajax({
    url: send_url,
    data: filter,
    success: function(data) {

      $(tag).html(data);
      
      if(tipe !== 'timeline'){
        $("#tabel_kinerja").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      }
      stopLoading();
    },
    error: function(xhr, status, error) {
     stopLoading();
     console(xhr.responseText);
     alert("Terjadi Kesalahan pada Sistem");
    }
  });
} 

/*
  tag = "#timeline"

  data: { 
    'start': start,
    'end' : end,
    'pegawai_id' : {{$pegawai->getPegawaiId()}}
  }
*/

function showTabelTahunanBulanan(pegawai_id, tahun, tag="#activity"){
  $.ajax({
    url: '/kinerja_tahunan_bawahan/tahunanbulanan/'+pegawai_id,
    data: { 
      'tahun': tahun
    },
    success: function(data) {
      $(tag).html(data);
    }
  });
} 

/*
tag = "#activity"
data: { 
    'tahun': tahun
},
*/

function filterTimeline(){    
    startLoading();
    tanggal_awal = $('#tanggal_awal').val();
    tanggal_akhir = $('#tanggal_akhir').val();
    pegawai_id = $('#pegawai').val();
    status = $('#status').val();
    var filter = { 
      'start': tanggal_awal,
      'end' : tanggal_akhir,
      'pegawai_id' : pegawai_id,
      'status' : status
    };
    showTimeline(filter, "#timeline", "tabel");
  }

dialog = $('#dialog-form').dialog({
  autoOpen: false,
  modal: true
});

function showDetail(detail_url, id){
  $('#btn-submit').hide();
  $.ajax({
    url: detail_url+id,
    success: function(data) {
      $("#dialog-form .modal-body").html(data);
      dialog.dialog('open');
    },
     error: function(xhr, status, error) {
        alert(xhr.responseText);
     }
  });
}

function showForm(detail_url){
  $('#btn-submit').show();
  $.ajax({
    url: detail_url,
    success: function(data) {
      $("#dialog-form .modal-body").html(data);
      dialog.dialog('open');
    },
     error: function(xhr, status, error) {
        alert(xhr.responseText);
     }
  });
}

function closeDialog(){
  dialog.dialog('close');
}

function showFormChangeStatus(base_url, id, status, base_id){
  url = base_url +'/?update='+id; 
  showForm(url);
  $('#btn-submit').unbind('click');
  $('#btn-submit').click(function(){
    komentar = $('#komentar').val();
    capaian_kualitas = 0;
    if(status == 2){
      capaian_kualitas = $('#capaian_kualitas').val();
    }
    changeStatus(base_url, id, status, base_id, komentar, capaian_kualitas);
    closeDialog();
  });
}

function changeStatus(base_url, id, status, base_id, komentar, capaian_kualitas){
  startLoading();
  $.ajax({
    type: 'POST',
    url: base_url+'?update='+id,
    headers: {
        'Content-Type' : 'application/x-www-form-urlencoded'
    },
    data: { 
        '_method': 'PATCH', 
        '_token': '{!! csrf_token() !!}',
        'save' : 1,
        'status' : status,
        'komentar' : komentar,
        'capaian_kualitas' : capaian_kualitas
    },
    success: function(data) {
      stopLoading();
      alert('Status berhasil diubah');
      if(base_id == 'harian_'){
        if(status == 2){
          $('#'+base_id+id).attr('class', 'fa fa-check bg-green');
        }
        else if(status == 3){
          $('#'+base_id+id).attr('class', 'fa fa-times bg-red');
        }
        else{
          $('#'+base_id+id).attr('class', 'fa fa-question bg-blue');
        }
      }
      else{
        if(status == 2){
          $('#'+base_id+id).attr('class', 'fa fa-check');
        }
        else if(status == 3){
          $('#'+base_id+id).attr('class', 'fa fa-times');
        }
        else{
          $('#'+base_id+id).attr('class', 'fa fa-question');
        }
      }
      filterTimeline();
      getNotifikasiSebagaiAtasan();
    },
     error: function(xhr, status, error) {
       stopLoading();
        console(xhr.responseText);
        alert("Sistem mengalami masalah");
     }
  });
}

/*
function accept(accept_url, id, base_id){
  changeStatus(accept_url, id, 1, base_id);
}

function reject(accept_url, id, base_id){
  changeStatus(accept_url, id, 2, base_id);
}*/


function accept(accept_url, id, base_id){
  showFormChangeStatus(accept_url, id, 2, base_id);
}

function reject(accept_url, id, base_id){
  showFormChangeStatus(accept_url, id, 3, base_id);
}