<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Pegawai;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pegawai = new Pegawai;
        $pegawai->nama = 'Administrator';
        $pegawai->nip = '00000';
        $pegawai->username = 'admin';
        $pegawai->password = 'admin';
        $pegawai->email = 'admin@ub.ac.id';
        $pegawai->jabatan_id = 1;
        $pegawai->instansi_id = 1;
        $pegawai->save();     
    }
}
