<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNilaiSkpBulananToDouble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nilai_skp_bulanan', function (Blueprint $table) {
            //
            $table->dropColumn(['nilai_tugas_jabatan', 'nilai_tugas_tambahan', 'nilai_skp']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nilai_skp_bulanan', function (Blueprint $table) {
            //
        });
    }
}
