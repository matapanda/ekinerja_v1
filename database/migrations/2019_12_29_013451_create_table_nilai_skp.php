<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNilaiSkp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_skp_bulanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pegawai_id');
            $table->integer('bulan');
            $table->integer('tahun');
            $table->decimal('nilai_tugas_jabatan',2);
            $table->decimal('nilai_tugas_tambahan',2);
            $table->decimal('nilai_skp',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nilai_skp_bulanan');
    }
}
