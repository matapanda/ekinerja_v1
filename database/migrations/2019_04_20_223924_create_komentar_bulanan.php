<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentarBulanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_kinerja_bulanan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('komentar');
            $table->integer('kinerja_bulanan_id');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar_kinerja_bulanan', function (Blueprint $table) {
            //
        });
    }
}
