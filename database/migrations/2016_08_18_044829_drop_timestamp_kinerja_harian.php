<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTimestampKinerjaHarian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kinerja_harian', function (Blueprint $table) {
            //
            $table->dropColumn('waktu_awal');
            $table->dropColumn('waktu_akhir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kinerja_harian', function (Blueprint $table) {
            //
        });
    }
}
