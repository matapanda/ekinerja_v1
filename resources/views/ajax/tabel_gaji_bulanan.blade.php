<div class="table-responsive">
    <table class="table">
        <thead>
	        <tr>
	            <th>Nama Kegiatan</th>
	            <th>Target Waktu</th>
				<th>Target Kuantitas</th>
				<th>Realisasi Kuantitas</th>
				<th>Realisasi Kualitas</th>
				<th>Realisasi Waktu</th>
				<th>Nilai Kinerja</th>
	        </tr>
       	</thead>
        <tbody>
        	@foreach($array_kinerja as $kb)
		@if($kb["jenis"] === 0)
	        <tr>
	            <td>{{ $kb["nama"] }}</td>
	            <td>{{ $kb["target_waktu"] }} hari</td>
	            <td>{{ $kb["target_kuantitas"] }}</td>
	            <td>{{ $kb["realisasi_kuantitas"] }}</td>
	            <td>{{ $kb["realisasi_kualitas"] }}</td>
		    <td>{{ $kb["realisasi_waktu"] }} hari</td>
	            <td>{{ number_format($kb["nilai_kinerja"],2) }}</td>
	    	</tr>
		@endif
	    	@endforeach
		<tr><th colspan="7">Kegiatan Tambahan</th></tr>
		@foreach($array_kinerja as $kb)
		@if($kb["jenis"] === 1)
	        <tr>
	            <td>{{ $kb["nama"] }}</td>
	            <td> - </td>
	            <td> - </td>
	            <td> - </td>
	            <td> - </td>
		    <td> - </td>
	            <td>{{ $kb["nilai_kinerja"] }}</td>
	    	</tr>
		@endif
	    	@endforeach
	    	<tr>
	            <td colspan="6" style="text-align: right;font-weight: bold">Nilai Tugas Jabatan</td>
	            <td>{{ number_format($nilai_tugas_jabatan,2) }}</td>
	    	</tr>
		<tr>
	            <td colspan="6" style="text-align: right;font-weight: bold">Nilai Tugas Tambahan</td>
	            <td>{{ $nilai_tugas_tambahan }}</td>
	    	</tr>
		<tr>
	            <td colspan="6" style="text-align: right;font-weight: bold">Nilai Capaian SKP Bulanan</td>
	            <td>{{ number_format($nilai_skp,2) }}</td>
	    	</tr>
	    	<!--
		<tr>
	    		<td colspan="6" style="text-align: right;font-weight: bold">Nilai Produktifitas</td>
	    		<td>-</td>
	    	</tr>
		    
	    	<tr>
	            <td colspan="5" style="text-align: right;font-weight: bold">Nilai Capaian Bulanan</td>
	            <td>-</td>
	    	</tr>
	    	<tr>
	            <td colspan="5" style="text-align: right;font-weight: bold">Tunjangan Dasar</td>
	            <td>Rp -</td>
	    	</tr>
	    	<tr>
	            <td colspan="5" style="text-align: right;font-weight: bold">Bobot Jabatan</td>
	            <td>-</td>
	    	</tr>
	    	<tr>
	            <td colspan="5" style="text-align: right;font-weight: bold">Tunjangan Diterima</td>
	            <td>Rp -</td>
	    	</tr>
		    -->
		</tbody>
    </table>
</div>