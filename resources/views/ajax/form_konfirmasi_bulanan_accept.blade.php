<div class="row">
            <div class="col-xs-12">
             

		                    <div class="form-group clearfix {!! $edit->field('target_kuantitas')->has_error !!}">
		                    <label class="col-sm-2 control-label{!! $edit->field('target_kuantitas')->req !!}">
                   				{!! $edit->field('target_kuantitas')->label !!}
                   			</label>
                   			<div class="col-sm-2">
		                        {!! $edit->field('target_kuantitas') !!}
                            <label>{!! $edit->field('target_kuantitas')->message !!}</label>
		                    </div>                   
		                    <div class="col-sm-5">
		                        {!! $edit->field('satuan_target_kuantitas') !!}
		                    </div>                        
		                    </div>

		                    <div class="form-group clearfix {!! $edit->field('capaian_kualitas')->has_error !!}">
		                    <label class="col-sm-2 control-label{!! $edit->field('capaian_kualitas')->req !!}">
                   				Capaian Kualitas
                   			</label>
		                    <div class="col-sm-2">
		                        {!! $edit->field('capaian_kualitas') !!}
		                    </div>
												<div class="col-sm-5">
		                        %
		                    </div>
		                    </div>

                            
                   			<label class="col-sm-2 control-label">
                   				Komentar
                   			</label>                   			
                   			<div class="col-sm-10">
                               <textarea class="form-control form-control" type="text" id="komentar" name="komentar" cols="50" rows="5"></textarea>
		                    </div>
		                    </div>
            </div><!-- /.col -->
          </div>