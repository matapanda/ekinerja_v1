<!-- The timeline -->
<ul class="timeline timeline-inverse">
  @foreach($kinerja_harian as $tanggal => $list_kinerja)
      <li class="time-label">
        <span class="bg-red">
          {{$tanggal}}
        </span>
      </li>
      @foreach($list_kinerja as $kinerja)
        <li>
        @if($kinerja->status == 1)
          <i class="fa fa-check bg-green" id="harian_{{$kinerja->id}}"></i>
        @elseif($kinerja->status == 2)
          <i class="fa fa-times bg-red" id="harian_{{$kinerja->id}}"></i>
        @else
          <i class="fa fa-exchange bg-blue" id="harian_{{$kinerja->id}}"></i>
        @endif
        
        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> {{$kinerja->waktu_awal}} - {{$kinerja->waktu_akhir}} </span>
          <span class="time"><i class="fa fa-book"></i> {{$kinerja->capaian_kuantitas}} {{$kinerja->satuan_kuantitas->nama }} </span>
          <h3 class="timeline-header">{{ $kinerja->nama }}</h3>
          <div class="timeline-body">
            <b> Deskripsi</b>
            {!! $kinerja->deskripsi !!}
          </div>
          <div class="timeline-footer">
            <a class="btn btn-primary btn-xs" href="#" onclick=showDetail('/keg_harian_peg_skpd/form',{{$kinerja->id}},'pegawai={{$kinerja->pegawai_id}}')>Detail</a>
            <a class="btn btn-success btn-xs" href="/keg_harian_peg_skpd/form?update={{$kinerja->id}}")>Ubah</a>
            <a class="btn btn-danger btn-xs" href="/keg_harian_peg_skpd/form?delete={{$kinerja->id}}&pegawai_id={{$kinerja->pegawai_id}}">Hapus</a>
          </div>
        </div>
      </li>
      @endforeach
  @endforeach
  <li>
    <i class="fa fa-clock-o bg-gray"></i>
  </li>
</ul>