<div class="col-sm-12">
<label class="col-sm-12 control-label">
Apakah anda yakin akan merubah status?
</label>
</div>

<br/>

@if($action == 'accept')
<div class="col-sm-12 {!! $edit->field('capaian_kualitas')->has_error !!}">
<label class="col-sm-2 control-label{!! $edit->field('capaian_kualitas')->req !!}">
  Capaian Kualitas
</label>
<div class="col-sm-10">                            
    {!! $edit->field('capaian_kualitas') !!}
    <label>{!! $edit->field('capaian_kualitas')->message !!}</label>
</div>
</div>
@endif

<div class="col-sm-12">
<label class="col-sm-12 control-label">
	Komentar
</label>
<div class="col-sm-12">  
<textarea class="form-control form-control" type="text" id="komentar" name="komentar" cols="50" rows="5"></textarea>
</div>
</div>