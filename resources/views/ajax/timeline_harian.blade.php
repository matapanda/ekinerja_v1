<!-- The timeline -->
<ul class="timeline timeline-inverse">
  @foreach($kinerja_harian as $tanggal => $list_kinerja)
      <li class="time-label">
        <span class="bg-red">
          {{$tanggal}}
        </span>
      </li>
      @foreach($list_kinerja as $kinerja)
        <li>
        @if($kinerja->status == App\Util::STATUS_SETUJU)
          <i class="fa fa-check bg-green" id="harian_{{$kinerja->id}}"></i>
        @elseif($kinerja->status == App\Util::STATUS_TOLAK)
          <i class="fa fa-times bg-red" id="harian_{{$kinerja->id}}"></i>
        @else
          <i class="fa fa-question bg-blue" id="harian_{{$kinerja->id}}"></i>
        @endif
        
        <div class="timeline-item">
          @if(!is_null($kinerja->bukti))
          <span class="time"><i class="fa fa-download"></i> <a href={{ URL::asset('ubk/'.$kinerja->bukti) }}>Bukti</a> </span>
          @endif
          <span class="time"><i class="fa fa-clock-o"></i> {{$kinerja->waktu_awal}} - {{$kinerja->waktu_akhir}} </span>
          @if($kinerja->jenis == 0)
          <span class="time"><i class="fa fa-book"></i> {{$kinerja->capaian_kuantitas}} {{$kinerja->satuan_kuantitas->nama }} </span>
          @endif
          <span class="time"><i class="fa fa-user"></i> {{$kinerja->nama_pegawai}} </span>
          <h3 class="timeline-header">{{ $kinerja->nama }}</h3>
          <div class="timeline-body">
            <b> Deskripsi</b>
            {!! $kinerja->deskripsi !!}
          </div>
          <!--
          <div class="timeline-footer">
            <a class="btn btn-primary btn-xs" href="#" onclick=showDetail('/kinerja_harian_bawahan/form?show=',{{$kinerja->id}})>Detail</a>
            <a class="btn btn-success btn-xs" href="#" onclick=accept('/kinerja_harian_bawahan/accept',{{$kinerja->id}},'harian_')>Setujui</a>
            <a class="btn btn-danger btn-xs" href="#" onclick=reject('/kinerja_harian_bawahan/reject',{{$kinerja->id}},'harian_')>Tolak</a>
          </div>
          -->
        </div>
      </li>
      @endforeach
  @endforeach
  <li>
    <i class="fa fa-clock-o bg-gray"></i>
  </li>
</ul>
