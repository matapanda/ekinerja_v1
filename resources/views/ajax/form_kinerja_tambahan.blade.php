
<div class="col-sm-12">
<label class="col-sm-4 control-label required">Hari</label>                			
<div class="col-sm-8">
	<div class="help-block">
  @if($edit->status == 'create')
    {!! $tanggal !!}
  @else
    {!! $edit->field('tanggal') !!}
  @endif
</div>
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('nama')->req !!}">
	{!! $edit->field('nama')->label !!}
</label>
<div class="col-sm-8">                            
  {!! $edit->field('nama') !!}
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('deskripsi')->req !!}">
{!! $edit->field('deskripsi')->label !!}
</label>                        
<div class="col-sm-8">
  @if($edit->field('deskripsi')->value == "")
    <i>Tidak ada deskripsi</i>
  @else    
    {!! $edit->field('deskripsi')->value !!}
  @endif
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('waktu_awal')->req !!}">
	{!! $edit->field('waktu_awal')->label !!}
</label>
<div class="col-sm-8">                            
  <div class="input-group"> 
    {!! $edit->field('waktu_awal')->value !!} - {!! $edit->field('waktu_akhir')->value !!}
  </div>              
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('bukti')->req !!}">
  {!! $edit->field('bukti')->label !!}
</label>
<div class="col-sm-8">
  @if($edit->field('bukti')->value)
    <a href="/ubk/{{ $edit->field('bukti')->value }}">Unduh Berkas</a>
  @else
    Tidak ada file bukti
  @endif
</div>
</div>

@if($edit->status == 'show')
<div class="col-sm-12">
  <label class="col-sm-4 control-label">
  Komentar Atasan
</label>
  <div class="col-sm-8">
  @foreach ($list_komentar as $komentar)    
    <p>
    {{ $komentar->created_at }}<br/>
    {{ $komentar->komentar }}
    </p>    
  @endforeach
  </div>
</div>
@endif
