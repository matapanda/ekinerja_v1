@extends('form')

@section('content')
<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                   {!! $edit->header !!}
                      {!! $edit->message !!}

                      @if(!$edit->message)
                        <div class="form-group clearfix {!! $edit->field('tanggal')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('tanggal')->req !!}">
                          {!! $edit->field('tanggal')->label !!}
                        </label>                        
                        <div class="col-sm-10">                            
                            {!! $edit->field('tanggal') !!}
                            <label>{!! $edit->field('tanggal')->message !!}</label>
                        </div>
                        </div>

                        @if(isset($nama_pegawai))
                        <div class="form-group clearfix {!! $edit->field('nama')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
                          Nama Pegawai
                        </label>                        
                        <div class="col-sm-10">
                            {!! $nama_pegawai !!}
                        </div>
                        </div>
                        @endif

                        <div class="form-group clearfix {!! $edit->field('nama')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
                          {!! $edit->field('nama')->label !!}
                        </label>
                        <div class="col-sm-10">                            
                            {!! $edit->field('nama') !!}
                            <label>{!! $edit->field('nama')->message !!}</label>
                        </div>

                        </div>

                        <div class="form-group clearfix {!! $edit->field('deskripsi')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('deskripsi')->req !!}">
                          {!! $edit->field('deskripsi')->label !!}
                        </label>                        
                        <div class="col-sm-10">                            
                            {!! $edit->field('deskripsi') !!}
                            <label>{!! $edit->field('deskripsi')->message !!}</label>
                        </div>
                        </div>

                        <div class="form-group clearfix ">
                        <div class="form-group col-sm-6 {!! $edit->field('waktu_awal')->has_error !!}">
                        <label class="col-sm-4 control-label{!! $edit->field('waktu_awal')->req !!}">
                          {!! $edit->field('waktu_awal')->label !!}
                        </label>
                        <div class="col-sm-8">                            
                            <div class="input-group"> 
                              {!! $edit->field('waktu_awal') !!}
                              @if($edit->status != 'show')
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              @endif
                            </div>
                            <label>{!! $edit->field('waktu_awal')->message !!}</label>                            
                        </div>
                        </div>

                        <div class="form-group col-sm-6 {!! $edit->field('waktu_akhir')->has_error !!}">
                        <label class="col-sm-4 control-label{!! $edit->field('waktu_akhir')->req !!}">
                          {!! $edit->field('waktu_akhir')->label !!}
                        </label>
                        <div class="col-sm-8">                            
                            <div class="input-group"> 
                              {!! $edit->field('waktu_akhir') !!}
                              @if($edit->status != 'show')
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              @endif
                            </div>
                            <label>{!! $edit->field('waktu_akhir')->message !!}</label>
                        </div>                        
                        </div>
                        </div>

                        <div class="form-group clearfix {!! $edit->field('kinerja_bulanan')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('kinerja_bulanan')->req !!}">
                          {!! $edit->field('kinerja_bulanan')->label !!}
                        </label>
                        <div class="col-sm-5">
                            {!! $edit->field('kinerja_bulanan') !!}
                            <label>{!! $edit->field('kinerja_bulanan')->message !!}</label>
                        </div>    
                        </div>

                        <div class="form-group clearfix {!! $edit->field('capaian_kuantitas')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('capaian_kuantitas')->req !!}">
                          {!! $edit->field('capaian_kuantitas')->label !!}
                        </label>
                        <div class="col-sm-5">
                            {!! $edit->field('capaian_kuantitas') !!}
                            <label>{!! $edit->field('capaian_kuantitas')->message !!}</label>
                        </div>                   
                        <div class="col-sm-5">
                            <div id="nama_satuan_kuantitas" style="padding-top: 8px"></div>
                            {!! $edit->field('satuan_target_kuantitas') !!}
                        </div>                        
                        </div>

                        <!--<div class="form-group clearfix {!! $edit->field('bukti')->has_error !!}">
                        <label class="col-sm-2 control-label{!! $edit->field('bukti')->req !!}">
                          {!! $edit->field('bukti')->label !!}
                        </label>
                        <div class="col-sm-10">                            
                            {!! $edit->field('bukti') !!}
                            <label>{!! $edit->field('bukti')->message !!}</label>
                        </div>
                        </div>-->
                        
                      @endif
                      <br/>                       
                   {!! $edit->footer !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datetimepicker/datetimepicker3.css') }}">
@endsection

@section('extra_js')
<script src="{{ asset('admin-lte/plugins/datetimepicker/bootstrap-datetimepicker.js') }}"></script>

<script src="{{ asset('admin-lte/plugins/datetimepicker/locales/bootstrap-datetimepicker.id.js') }}"></script>

<script type="text/javascript">
var json_bulanan = {!! $json_kinerja_bulanan !!};

var selected_kinerja_bulanan_id = {!! $selected_kinerja_bulanan_id !!};

function changeSatuanBulanan(id_bulanan){
  var length = Object.keys(json_bulanan).length;
  
  $("input[name='satuan_target_kuantitas']").val(0);
  $("#nama_satuan_kuantitas").html("");

  //alert(length);
  if(length>0){
    id_jumlah = json_bulanan[id_bulanan].id_jumlah;
    id_waktu = json_bulanan[id_bulanan].id_waktu;
    nama_jumlah = json_bulanan[id_bulanan].nama_jumlah;
    nama_waktu = json_bulanan[id_bulanan].nama_waktu;

    $("input[name='satuan_target_kuantitas']").val(id_jumlah);
    $("#nama_satuan_kuantitas").html(nama_jumlah);
  }
}

$(document).ready(function () {
  
});

</script>

<script type="text/javascript">
  
  function refreshDropdownKinerjaBulanan(){
    startLoading();
    var tgl = $('#tanggal').val();

    var $el = $("#kinerja_bulanan");
    $el.empty();
    $el.focus();
    $el.blur();
   
    $.ajax({
      url: '/kinerja_harian/daftarkinerjabulanan',
      data : {
        'tanggal' : $('#tanggal').val()
      },      
      success: function(data) {
        stopLoading();
        json_bulanan = data;
        
        var $el = $("#kinerja_bulanan");
        $el.empty();
                
        for(key in data){   
          $el.append($("<option></option>")
                    .attr("value", key).text(data[key].nama_kegiatan));
        }

        if(selected_kinerja_bulanan_id != 0){
          $('#kinerja_bulanan option[value="' + selected_kinerja_bulanan_id + '"]').prop('selected', true);
        }

        id_bulanan = $('#kinerja_bulanan').val();
        changeSatuanBulanan(id_bulanan);
      },
      error: function(xhr, status, error) {
        stopLoading();
        console(xhr.responseText);
        alert("Terjadi Kesalahan pada Sistem");
      }
    });
   }
</script>

<script language="javascript" type="text/javascript">

$(document).ready(function () {
                        $('#tanggal').datetimepicker({
                            format: 'dd-mm-yyyy',
                            language: 'id',
                            todayBtn: 'linked',
                            autoclose: true,
                            minView : 2,
                            maxView : 2,
                            startView : 2,
                            isInline : true,
                            defaultDate : new Date()
                        }).on('changeDate', function(ev){
                          refreshDropdownKinerjaBulanan();
                        });
 
                        $('#waktu_awal').datetimepicker({
                            format: 'hh:ii:00',
                            language: 'id',
                            todayBtn: 'linked',
                            autoclose: true,
                            minView : 0,
                            maxView : 0,
                            startView : 1,
                            minuteStep : 5,
                            isInline : true,
                            defaultDate : new Date()
                        });

                        $('#waktu_akhir').datetimepicker({
                            format: 'hh:ii:00',
                            language: 'id',
                            todayBtn: 'linked',
                            autoclose: true,
                            pickTime: false,
                            minView : 0,
                            maxView : 0,
                            startView : 1,
                            minuteStep : 5,
                            defaultDate : new Date()
                        }); 

                        id_bulanan = $('#kinerja_bulanan').val();
                        changeSatuanBulanan(id_bulanan);
                        $('select').on('change', function() {
                          id_bulanan = $('#kinerja_bulanan').val();
                          changeSatuanBulanan(id_bulanan);
                        });

                        refreshDropdownKinerjaBulanan();

});
</script>




@endsection