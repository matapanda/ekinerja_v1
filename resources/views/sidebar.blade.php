<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">  
              @if( $pegawai_global->foto && file_exists( public_path().'/upload/'.$pegawai_global->foto) )
              <img src="{{ asset('upload/'.$pegawai_global->foto) }}" class="img-circle" alt="User Image">
              @else           
              <img src="{{ asset('user.jpg') }}" class="img-circle" alt="User Image">
              @endif
            </div>
            <div class="pull-left info">
              
              <p>{{ $pegawai_global->nama }}</p>
                 {{ $pegawai_global->nip_baru }}
            </div>
          </div>

          <div class="info" style="color: #fff;padding-left: 7px;">

                 {{ $pegawai_global->getNamaJabatan() }}

          </div>

          <div style="color: #fff; text-align: center;padding-top: 5px;padding-bottom: 5px;">
                 <!-- { $pegawai_global->getNamaUnitSubUnitK() } -->
          </div>
          <!-- search form -->
          <!--
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview {{ in_array(Request::segment(1), array('/')) ? 'active' : '' }}">
              <a href="{{ url('/') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>              
            </li>
            @if(Auth::user()->hasRole('admin-skpd'))
              <ul class="treeview-menu">
                <li class="{{ Request::is('atasanbawahan*')  ? 'active' : '' }}"><a href="{{ url('atasanbawahan') }}">Atasan dan Bawahan</a></li>
              </ul>
            @endif
            
            <li class="treeview {{ Request::is('kinerja_tahunan*') ? 'active' : '' }}">
              <a href="{{ url('kinerja_tahunan') }}"><i class="fa fa-calendar"></i> <span>Kegiatan Tahunan</span></a>
            </li>

            <li class="treeview {{ Request::is('kinerja_bulanan*') ? 'active' : '' }}">
              <a href="{{ url('kinerja_bulanan') }}"><i class="fa fa-calendar-plus-o"></i> <span>Kegiatan Bulanan</span></a>
            </li>

            <li class="treeview {{ Request::segment(1) == 'kinerja_harian' ? 'active' : '' }}">
              <a href="{{ url('kinerja_harian') }}"><i class="fa fa-calendar-o"></i> <span>Kegiatan Harian</span></a>
            </li>

            @if($pegawai_global->hasBawahan())
            <li class="treeview {{ Request::segment(1) == 'kegiatan_bulanan_bawahan' || Request::segment(1) == 'kinerja_harian_bawahan' ? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-users"></i> <span>Pegawai Bawahan</span>
              </a>
              <!--
              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'bawahan' ? 'active' : '' }}">
                <a href="{{ url('bawahan') }}"><span>Profil Pegawai</span></a>
              </li>
              </ul>
              
              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'bawahan' ? 'active' : '' }}">
                <a href="{{ url('bawahan') }}"><span>Daftar Bawahan</span></a>
              </li>
              </ul>
              
              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'kinerja_tahunan_bulanan_bawahan' ? 'active' : '' }}">
                <a href="{{ url('kinerja_tahunan_bulanan_bawahan') }}"><span>Rencana Keg. Tahunan Bawahan</span></a>
              </li>
              </ul>
              -->
              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'kinerja_harian_bawahan' ? 'active' : '' }}">
                <a href="{{ url('kinerja_harian_bawahan') }}"><span>Kegiatan Harian Bawahan</span></a>
              </li>
              </ul>
              

              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'kegiatan_bulanan_bawahan' ? 'active' : '' }}">
                <a href="{{ url('kegiatan_bulanan_bawahan') }}"><span>Kegiatan Bulanan Bawahan</span></a>
              </li>
              </ul>        
            </li>
            @endif
            
            <li class="treeview {{ Request::segment(1) == 'report' || Request::segment(1) == 'tunjangan' ? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-bar-chart"></i> <span>Laporan</span>
              </a>
              <!--
              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'report' ? 'active' : '' }}">
                <a href="{{ url('report') }}"><span>Laporan Kinerja</span></a>
              </li>
              </ul>
              -->
              <ul class="treeview-menu">
              <li class="treeview {{ Request::segment(1) == 'tunjangan' ? 'active' : '' }}">
                <a href="{{ url('tunjangan') }}"><span>Nilai SKP Bulanan</span></a>
              </li>
              </ul>
            </li> 

            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>