@extends('base')

@section('page_title')
Dashboard
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datepicker/datepicker3.css') }}">
@endsection

@section('extra_js')
<script src="{{ asset('admin-lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('admin-lte/dist/js/raphael-min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/morris/morris.min.js') }}"></script>
<script type="text/javascript">
function getStatistik(){
  $.ajax({
    url: '/dashboard/statistik',
    
    success: function(data) {
      stat = JSON.parse(data);
      $('#jumlah_kegiatan_tahun_ini').html(stat['jumlah_kegiatan_tahunan']);
      $('#jumlah_kegiatan_bulan_ini').html(stat['jumlah_kegiatan_bulanan']);
    }
  });
} 

function requestData(tipe, tahun, id_peg, chart){

    $.ajax({
      type: "GET",
      url: "{{url('getReport/')}}", // This is the URL to the API
      data: { tipe: tipe, tahun:tahun, id_peg:id_peg }
    })
    .done(function( data ) {
      // When the response to the AJAX request comes back render the chart with new data
      chart.setData(JSON.parse(data));
      chart.options.yLabelFormat= function (y) { 
            if(tipe==1) return (Math.round( y * 10 ) / 10).toString() + ' kali';
            else if(tipe==2) return (Math.round( y * 10 ) / 10).toString() + ' %';
            else if(tipe==3) return (Math.round( y * 10 ) / 10).toString() + ' %';
            else if(tipe==4) return (Math.round( y * 10 ) / 10).toString() + ' Jam';
          };
      chart.redraw();
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });
  }

$(document).ready(function () {    

    getStatistik();

    //The Calender
    $("#calendar").datepicker();

      
    var line = Morris.Bar({
    // ID of the element in which to draw the chart.
    element: 'line-chart',
    // Set initial data (ideally you would provide an array of default data)
    data: [0,0],
    xkey: 'nama_bulan',
    ykeys: ['nilai'],
    labels: ['Nilai'],
    hideHover: 'auto',
    xLabelAngle:'45'
  });

    requestData(1, 0, 0, line);
    
  });
</script>
@endsection

@section('content')
<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3 id="jumlah_kegiatan_tahun_ini">0</h3>
                  <p>Kegiatan Tahun {{ $tahun }}</p>
                </div>
                <div class="icon">
                  <i class="ion ion-calendar"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3 id="jumlah_kegiatan_bulan_ini">0</h3>
                  <p>Kegiatan Bulan {{$bulan}} {{$tahun}}</p>
                </div>
                <div class="icon">
                  <i class="ion ion ion-stats-bars"></i>
                </div>
                
              </div>
            </div>

            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3 class="jumlah_notifikasi_pegawai">0</h3>
                  <p>Kegiatan Harian Ditolak Atasan</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>                
              </div>
            </div>

            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3 class="jumlah_notifikasi_atasan">0</h3>
                  <p>Kegiatan Butuh Konfirmasi Anda</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person"></i>
                </div>                
              </div>
            </div>
            
            </div>
            
          
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <!-- solid sales graph -->
          <div class="box box-solid bg-teal-gradient">
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">Jumlah Jam Kinerja Harian</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body border-radius-none">
              <div class="chart" id="line-chart" style="height: 250px;"></div>
            </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
              <!-- /.nav-tabs-custom -->

                        <!-- solid sales graph -->
              <!--
              <div class="box box-solid bg-teal-gradient">
                <div class="box-header">
                  <i class="fa fa-th"></i>

                  <h3 class="box-title">Laporan Pencapaian</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body border-radius-none">
                  <div class="chart" id="line-chart" style="height: 250px;"></div>
                </div>               
                
              </div>
              
              -->

          
              

            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

              <!-- TO DO List -->
              <!-- Calendar -->
              <div class="box box-solid bg-green-gradient">
                <div class="box-header">
                  <i class="fa fa-calendar"></i>

                  <h3 class="box-title">Calendar</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <!-- button with a dropdown -->
                    <div class="btn-group">
                      <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bars"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="#">Add new event</a></li>
                        <li><a href="#">Clear events</a></li>
                        <li class="divider"></li>
                        <li><a href="#">View calendar</a></li>
                      </ul>
                    </div>
                    <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                  <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <!--The calendar -->
                  <div id="calendar" style="width: 100%"></div>
                </div>
                <!-- /.box-body -->
                
              </div>
              <!-- /.box -->

             
            </section>
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
@endsection

<!-- jQuery 2.2.3 -->
@section('extra_js_old')



<script>
$(function() {
  // Create a function that will handle AJAX requests
  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/kegiatanTahunan')}}"
  })
  .done(function( data ) {
    // When the response to the AJAX request comes back render the chart with new data
    document.getElementById('jumlah_kegiatan_tahun_ini').innerHTML=data;
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });
  
  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/kegiatanBulanan')}}"
  })
  .done(function( data ) {
    // When the response to the AJAX request comes back render the chart with new data
    document.getElementById('jumlah_kegiatan_bulan_ini').innerHTML=data;
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });
  
  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/capaianKuantitasTahunan')}}"
  })
  .done(function( data ) {
    // When the response to the AJAX request comes back render the chart with new data
    document.getElementById('capaian_kuantitas_tahun_ini').innerHTML=data+'<sup style="font-size: 20px">%</sup>';
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });

  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/capaianKualitasTahunan')}}"
  })
  .done(function( data ) {
    // When the response to the AJAX request comes back render the chart with new data
    document.getElementById('capaian_kualitas_tahun_ini').innerHTML=data+'<sup style="font-size: 20px">%</sup>';
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });  

  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/listKegiatanBulanan')}}"
  })
  .done(function( data ) {
    
    $.each(data, function(key, item) {
      $tambahan='';
      if(item.status==1) $tambahan='<li class="done">';
      else $tambahan='<li class="">';
      $tambahan = $tambahan+'<span class="handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i>  </span> <span class="text">'+item.nama+'</span> <small class="label label-warning"><i class="fa fa-clock-o"></i> '+item.target_waktu+' '+item.nama_satuan_waktu+' </small> <small class="label label-primary"><i class="fa fa-signal"></i> '+item.target_kuantitas+' '+item.nama_satuan_jumlah+' </small> </li>';
      $('#list_kegiatan').append($tambahan);
    });
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });

  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/laporanJumlahBulanan')}}"
  })
  .done(function( data ) {
    
    $.each(data, function(key, item) {
      
    });
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });

  $.ajax({
    type: "GET",
    url: "{{url('/dashboard/laporanCapaianBulanan')}}"
  })
  .done(function( data ) {
    
    $.each(data, function(key, item) {
      
    });
  })
  .fail(function() {
    // If there is no communication between the server, show an error
    //alert( "error occured" );
  });

});
</script>

@endsection