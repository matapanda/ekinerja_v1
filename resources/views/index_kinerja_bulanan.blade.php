@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_js')

<script type="text/javascript">
  
  $(document).ready(function () {
    dialog = $('#dialog-form').dialog({
      autoOpen: false,
      modal: true
    });    
  });

  function showDetail(detail_url, id){
    $.ajax({
      url: detail_url+id,
      success: function(data) {
        $("#dialog-form .modal-body").html(data);
        dialog.dialog('open');
      }
    });
  }

  function closeDialog(){
    dialog.dialog('close');
  }
</script>>
@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                  @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                  <div class="pull-right">
                  <a class="btn btn-warning" href="kinerja_bulanan/form" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px"><i class="fa fa-plus"></i> Kegiatan </a>
                  <a class="btn btn-default" href="kinerja_bulanan/formtambahan" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px"><i class="fa fa-plus"></i> Kegiatan Tambahan</a>
                  </div>
                  {!! $filter !!}
                  {!! $grid !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection