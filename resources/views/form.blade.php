@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li><a href={{ $base_url }}>{{ $page_title }}</a></li>
            <li class="active">{{ $page_activity }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
{{ $page_activity }}
@endsection


@section('content')

<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  {!! $edit !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection