@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li><a href={{ $base_url }}>{{ $page_title }}</a></li>
            <li class="active">{{ $edit->field('nama')->value }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
{{ $page_activity }}
@endsection


@section('content')
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  {!! $edit !!}
                  <div class="col-lg-12">
                    
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#activity" data-toggle="tab"><span class="glyphicon glyphicon-book"></span> Kegiatan Harian</a></li>
                      <li><a href="#timeline" data-toggle="tab"><span class="glyphicon glyphicon-calendar"></span> Rancangan Kinerja Bulanan</a></li>
                      <li><a href="#settings" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Rancangan Kinerja Tahunan</a></li>
                    </ul>
                    
                    <div class="active tab-pane" id="activity">
                      <ul class="timeline timeline-inverse">
                        <li class="time-label"><span class="bg-red">
                          10 Feb. 2014</span>
                        </li>
                        <li>
                        <i class="fa fa-envelope bg-blue"></i>
                        <div class="timeline-item">
                          <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                          <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
                          <div class="timeline-body">
                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                            quora plaxo ideeli hulu weebly balihoo...
                          </div>
                          <div class="timeline-footer">
                            <a class="btn btn-primary btn-xs">Read more</a>
                            <a class="btn btn-danger btn-xs">Delete</a>
                          </div>
                        </div>
                      </li>
                      </ul>
                    </div>

                    
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->



          </div>
@endsection