@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li><a href={{ $base_url }}>{{ $page_title }}</a></li>
            <li class="active">{{ $pegawai->nama }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
{{ $pegawai->nama }}
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datetimepicker/datetimepicker3.css') }}">
@endsection

@section('extra_js')
<script src="{{ asset('admin-lte/plugins/datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datetimepicker/locales/bootstrap-datetimepicker.id.js') }}"></script>
<script type="text/javascript">
  function showTimeline(start, end){
    $.ajax({
      url: '/keg_harian_peg_skpd/timelineharian/{{$pegawai->getPegawaiId()}}',
      data: { 
          'start': start,
          'end' : end
      },
      success: function(data) {
        $("#timeline").html(data);
      }
    });
  } 

  function showTabelTahunanBulanan(tahun){
    $.ajax({
      url: '/keg_tahunan_bulanan_peg_skpd/tahunanbulanan?pegawai_id={{$pegawai->getPegawaiId()}}',
      data: { 
          'tahun': tahun
      },
      success: function(data) {
        $("#activity").html(data);
      }
    });
  } 


  $(document).ready(function () {
    dialog = $('#dialog-form').dialog({
      autoOpen: false,
      modal: true
    });

     $('#tanggal_awal').datetimepicker({
        format: 'yyyy-mm-dd',
        language: 'id',
        todayBtn: 'linked',
        autoclose: true,
        minView : 2,
        maxView : 2,
        startView : 2,
        isInline : true
    });

    $('#tanggal_akhir').datetimepicker({
        format: 'yyyy-mm-dd',
        language: 'id',
        todayBtn: 'linked',
        autoclose: true,
        minView : 2,
        maxView : 2,
        startView : 2,
        isInline : true
    });

    today = moment().format('YYYY-MM-DD');
    prevDay = moment().subtract(9 ,'days').format('YYYY-MM-DD');
    $('#tanggal_awal').val(prevDay);
    $('#tanggal_akhir').val(today);
    showTimeline(prevDay, today);

    tahun = $('#tahun').val();
    showTabelTahunanBulanan(tahun);    
    
  });

  function filterTimeline(){    
    tanggal_awal = $('#tanggal_awal').val();
    tanggal_akhir = $('#tanggal_akhir').val();
    showTimeline(tanggal_awal, tanggal_akhir);
  }

  function filterTabelTahunanBulanan(){ 
    tahun = $('#tahun').val();
    showTabelTahunanBulanan(tahun);
  }

  
</script>
<script src="{{ asset('js/ajax_crud_kinerja.js') }}"></script>
@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">                  
                  <div class="col-md-3">               
                  @if($pegawai->foto)
                  <img src="{{ asset('upload/'.$pegawai->foto) }}" class="profile-user-img img-responsive img-circle" alt="User Image">
                  @else
                  <img src="{{ asset('user.jpg') }}" class="profile-user-img img-responsive img-circle" alt="User Image">
                  @endif
                  <h3 class="profile-username text-center">{{ $pegawai->nama }}</h3>
                  <p class="text-muted text-center">{{ $pegawai->nip }}</p>
                  </div>
                  
                  <div class="col-md-9">
                  <strong><i class="fa fa-bookmark margin-r-5"></i> Jabatan</strong>
                  <p class="text-muted">{{ $pegawai->jabatan->nama }}
                   {{ $pegawai->instansi->nama }}</p>
                  
                  <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                  <p class="text-muted">{{ $pegawai->email }}</p>  
                  
                  <strong><i class="fa fa-book margin-r-5"></i>  Pendidikan Terakhir</strong>
                  <p class="text-muted">
                    {{ $pegawai->pendidikan }}
                  </p>
                  
                  <!--
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
                  <p class="text-muted">{{ $pegawai->alamat }}</p>  
                  -->
                  </div>                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->              
            </div><!-- /.col -->

            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li @if($active=='harian') class="active" @endif><a href="#timeline_tab" data-toggle="tab" aria-expanded="true">Kegiatan Harian</a></li>
                  <li @if($active=='tahunan') class="active" @endif><a href="#activity_tab" data-toggle="tab" aria-expanded="true">Kinerja Tahunan dan Bulanan</a></li>
                  
                </ul>

                <div class="tab-content">                  
                  <div class="tab-pane @if($active=='harian') active @endif" id="timeline_tab">
                    <input class="form-control" type="text" id="tanggal_awal" style="width:30%; display: inline" placeholder="Tanggal Awal">
                    <input class="form-control" type="text" id="tanggal_akhir" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px" placeholder="Tanggal Akhir">

                    <a class="btn btn-primary" href="#" onclick="filterTimeline()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Filter</a>

                    <a class="btn btn-warning" href="/keg_harian_peg_skpd/form?pegawai_id={{$pegawai->getPegawaiId()}}" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px"> 
                    <i class="fa fa-plus"> </i> Kegiatan Harian</a>

                    <div id="timeline" style="margin-top: 30px">

                    </div>
                  </div><!-- /.tab-pane --> 

                  <div class="tab-pane @if($active=='tahunan') active @endif"  id="activity_tab">
                   {!! Form::select('tahun', $list_tahun, null, $options = array('id' => 'tahun', 'class' => 'form-control', 'style'=>'width:30%; display: inline')) !!}

                   <a class="btn btn-primary" href="#" onclick="filterTabelTahunanBulanan()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Filter</a>
                   

                   <a class="btn btn-primary pull-right" href="#" onclick="showInsertForm('/keg_tahunan_bulanan_peg_skpd/form','','&pegawai_id={{$pegawai->id}}')" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">
                    <i class="fa fa-plus"></i> Kegiatan Tahunan</a>
                   

                   <div id="activity" style="margin-top: 30px">

                   </div>
                  </div>          
                </div><!-- /.tab-content -->


              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div>
@endsection