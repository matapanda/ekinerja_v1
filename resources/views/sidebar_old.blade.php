<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              @if(Auth::user()->foto)
              <img src="{{ asset('upload/'.Auth::user()->foto) }}" class="img-circle" alt="User Image">
              @else
              <img src="{{ asset('user.jpg') }}" class="img-circle" alt="User Image">
              @endif
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->nama }}</p>
             {{ Auth::user()->nip }}
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview {{ in_array(Request::path(), array('/')) ? 'active' : '' }}">
              <a href="{{ url('/') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>              
            </li>
            @if(Auth::user()->hasRole('superadmin'))
            <li class="treeview {{ Request::is('jabatan*') || Request::is('instansi*') || Request::is('pegawai*')? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Induk</span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Request::is('jabatan*')  ? 'active' : '' }}"><a href="{{ url('jabatan') }}">Jabatan</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('instansi*')  ? 'active' : '' }}"><a href="{{ url('instansi') }}">Instansi</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('pegawai*')  ? 'active' : '' }}"><a href="{{ url('pegawai') }}">Pegawai</a></li>
              </ul>
            </li>
            @endif
            <li class="treeview {{ Request::is('kinerja_tahunan*') || Request::is('kinerja_bulanan*') ? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-user"></i> <span>Rencana Kinerja</span>
              </a>    
                <ul class="treeview-menu">
                  <li class="{{ Request::is('kinerja_tahunan*')  ? 'active' : '' }}"><a href="{{ url('kinerja_tahunan') }}">Kinerja Tahunan</a>
                  </li>
                  <li class="{{ Request::is('kinerja_bulanan*')  ? 'active' : '' }}"><a href="{{ url('kinerja_bulanan') }}">Kinerja Bulanan</a>
                  </li>
              </ul>          
            </li>
            <li class="treeview {{ Request::is('kinerja_harian*') ? 'active' : '' }}">
              <a href="{{ url('kinerja_harian') }}"><i class="fa fa-calendar"></i> <span>Kegiatan Harian</span></a>
            </li>

            @if(Auth::user()->jabatan->berwenang)
            <li class="treeview {{ Request::is('bawahan*') ? 'active' : '' }}">
              <a href="{{ url('bawahan') }}"><i class="fa fa-users"></i> <span>Pegawai Bawahan</span></a>
            </li>         
            @endif
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>