@extends('form_kinerja_harian')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li><a href="/peg_skpd/show/{!! $id_pegawai !!}">{{ $nama_pegawai }}</a></li>
            <li class="active">{{ $page_activity.' '.$page_title }} </li>
@endsection