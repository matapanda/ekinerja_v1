@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
@endsection

@section('extra_js')
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
<script type="text/javascript">
  function showTabelTahunanBulanan(tahun, posisi, pegawai){
    $.ajax({
      url: '/kinerja_tahunan_bulanan_bawahan/tahunanbulanan',
      data: { 
          'pegawai' : pegawai,
          'tahun': tahun,
          'posisi': posisi
      },
      success: function(data) {
        $("#tabel-kinerja").html(data);
      }
    });
  } 

  function showTab(){
    $('#tab').show();
  }

  function filterTabelTahunanBulanan(){ 
    tahun = $('#tahun').val();
    posisi = $('#posisi').val();
    pegawai = $('#pegawai').val();
    showTabelTahunanBulanan(tahun, posisi, pegawai);
  }

  function getProfilPegawai(){
    pegawai = $('#pegawai').val();
    if(pegawai != null){
      $.ajax({
        url: '/kinerja_tahunan_bulanan_bawahan/profilpegawai',
        data: { 
            'pegawai' : pegawai
        },      
        success: function(data) {
          $("#profil").html(data);
          showTab();
        }
      });
    }    
  }

  function doAction(){
    filterTabelTahunanBulanan();    
  }

  function refreshDropdownTahun(){
    pegawai = $('#pegawai').val();
    if(pegawai != null){
      $.ajax({
        url: '/kinerja_tahunan_bulanan_bawahan/tahun',
        data: { 
            'pegawai' : pegawai
        },      
        success: function(data) {
          var $el = $("#tahun");
          $el.empty();
          $.each(data, function(value, key) {            
              $el.append($("<option></option>")
                      .attr("value", value).text(key));
          });
          // Refresh tabel setelah refresh tahun
          //filterTabelTahunanBulanan();          
        }
      });
    }    
  }

  function requestData(tipe, tahun, id_peg, chart){
    $.ajax({
      type: "GET",
      url: "{{url('getReport/')}}", // This is the URL to the API
      data: { tipe: tipe, tahun:tahun, id_peg:id_peg }
    })
    .done(function( data ) {

      // When the response to the AJAX request comes back render the chart with new data
      chart.setData(JSON.parse(data));
      chart.options.yLabelFormat= function (y) { 
            if(tipe==1) return (Math.round( y * 10 ) / 10).toString() + ' kali';
            else if(tipe==2) return (Math.round( y * 10 ) / 10).toString() + ' %';
            else if(tipe==3) return (Math.round( y * 10 ) / 10).toString() + ' %';
            else if(tipe==4) return (Math.round( y * 10 ) / 10).toString() + ' Jam';
          };
      chart.redraw();
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });
  }     

  var chart = Morris.Bar({
        // ID of the element in which to draw the chart.
        element: 'stats-container',
        // Set initial data (ideally you would provide an array of default data)
        data: [0,0],
        xkey: 'nama_bulan',
        ykeys: ['nilai'],
        labels: ['Nilai'],
        hideHover: 'auto',
        xLabelAngle:'45'
      });

  function drawChart(){
      //e.preventDefault();
      $('#stats-container').show();
      // Get the number of days from the data attribute
      tipe = $('#tipe').val();
      if(tipe==1) $("#title").html("Laporan Jumlah Kegiatan Harian");
      else if (tipe==2) $("#title").html("Laporan Rata-rata Kualitas Kegiatan");
      else if (tipe==3) $("#title").html("Laporan Persentase Capaian Kuantitas Kegiatan");
      else if (tipe==4) $("#title").html("Laporan Jumlah jam kerja");
      var tahun = $('#tahun').val();
      var id_peg = $('#pegawai').val();
      // Request the data and render the chart using our handy function


      requestData(tipe, tahun, id_peg, chart);
    } 


  $(document).ready(function () {
      dialog = $('#dialog-form').dialog({
        autoOpen: false,
        modal: true,
        width: 100,
        maxWidth: 100
      });

      $('#pegawai').change(function(){
        refreshDropdownTahun();
        getProfilPegawai();
      });

      
  });

  
</script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/ajax_crud_kinerja.js') }}"></script>

@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;top:0;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div>
  </div>
</div>
<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                  <div class="pull-left" style="width:100%">
                  {!! Form::select('pegawai', $list_pegawai, null, $options = array('id' => 'pegawai', 'class' => 'form-control', 'style'=>'width:20%; display: inline; margin-left: 10px; margin-left: 10px')) !!}
                   </div>

                      <div id="profil" style="padding-top: 50px">
                      </div>

                      <div id="tab" class="col-md-12" style="padding-top: 10px; display: none">
                        <div class="nav-tabs-custom">
                          <ul class="nav nav-tabs">
                            <li class="active"><a href="#timeline_tab" data-toggle="tab" aria-expanded="true">Rencana Kegiatan Tahunan</a></li>
                            <li class=""><a href="#activity_tab" data-toggle="tab" aria-expanded="false">Statistik Kinerja</a></li>                            
                          </ul>

                          <div class="tab-content">                  
                            <div class="tab-pane active" id="timeline_tab">
                              {!! Form::select('tahun', array(), null, $options = array('id' => 'tahun', 'class' => 'form-control', 'style'=>'width:20%; display: inline')) !!}
                              <a class="btn btn-success" href="#" onclick="doAction()" style="width:10%; display: inline; margin-left: 10px; margin-left: 10px">Tampilkan</a>
                              <div id="tabel-kinerja" style="padding-top: 10px">
                              </div>
                            </div><!-- /.tab-pane --> 

                            <div class="tab-pane"  id="activity_tab">              
                            <select id="tipe" class="form-control" style="width: 30%; display: inline;">
                              <option value="1">Laporan Jumlah Kegiatan Harian</option>
                              <option value="2">Laporan Rata-rata Kualitas Kegiatan</option>
                              <option value="3">Laporan Rata-rata Capaian Kuantitas Kegiatan</option>
                              <option value="4">Laporan Jumlah jam kerja</option>
                            </select>

                            <a class="btn btn-success" href="#" onclick="drawChart()" style="width:10%; display: inline; margin-left: 10px; margin-left: 10px">Tampilkan</a>
                              

                              <div class="row">
                                <div class="col-lg-12" > 
                                  <br>
                                  <br>
                                  <h3 class="box-title" id="title" style="text-align:center; width: 100%; margin: 0 auto;" ></h3>
                                  <br>
                                  <br>
                                  
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12"> 
                                  <div id="stats-container" style="height: 250px; text-align:center; width: 90%; margin: 0 auto; display:none;"></div>
                                </div>
                              </div>
                            </div>          
                          </div><!-- /.tab-content -->
                        </div><!-- /.nav-tabs-custom -->
                      </div><!-- /.col -->

                      

                      
                      
                      
                   </div>                    
                   </div>
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection