<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">                         
              <img src="{{ asset('user.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              
              <p>{{ Auth::user()->username }}</p>
              @if(Auth::user()->isAdminSKPD())
                Administrator SKPD
              @endif
              @if(Auth::user()->isSuperAdmin())
                Super Admin
              @endif
            </div>
          </div>

          <div style="color: #fff; text-align: center;padding-top: 5px;padding-bottom: 5px;">
                 {!! Auth::user()->getNamaUnitSubUnitK() !!}
          </div>
          <!-- search form -->
          <!--
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview {{ in_array(Request::segment(1), array('/')) ? 'active' : '' }}">
              <a href="{{ url('/') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>              
            </li>
            
            
            <li class="treeview {{ Request::is('jabatan*') || Request::is('instansi*') || Request::is('pegawai*') || Request::is('satuan_jumlah*') || Request::is('satuan_waktu*') || Request::is('atasanbawahan*') || Request::is('bobot_jabatan*') || Request::is('klasifikasi_bobot_jabatan*') || Request::is('setting*') || Request::is('admin_nilai_skp_bulanan*')? 'active' : '' }}">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Induk</span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ Request::is('user*')  ? 'active' : '' }}"><a href="{{ url('user') }}">User</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('atasanbawahan*')  ? 'active' : '' }}"><a href="{{ url('atasanbawahan') }}">Atasan dan Bawahan</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('klasifikasi_bobot_jabatan*')  ? 'active' : '' }}"><a href="{{ url('klasifikasi_bobot_jabatan') }}">Klasifikasi Bobot Jabatan Pegawai</a></li>
              </ul>
              @if(Auth::user()->isSuperAdmin())
              <ul class="treeview-menu">
                <li class="{{ Request::is('admin_nilai_skp_bulanan*')  ? 'active' : '' }}"><a href="{{ url('admin_nilai_skp_bulanan') }}">Nilai SKP Bulanan</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('bobot_jabatan*')  ? 'active' : '' }}"><a href="{{ url('bobot_jabatan') }}">Bobot Jabatan</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('satuan_jumlah*')  ? 'active' : '' }}"><a href="{{ url('satuan_jumlah') }}">Satuan Jumlah</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('satuan_waktu*')  ? 'active' : '' }}"><a href="{{ url('satuan_waktu') }}">Satuan Waktu</a></li>
              </ul>
              <ul class="treeview-menu">
                <li class="{{ Request::is('setting*')  ? 'active' : '' }}"><a href="{{ url('setting') }}">Setting</a></li>
              </ul>
              @endif
            </li>
            

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>