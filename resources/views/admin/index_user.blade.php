@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection


@section('extra_js')

<script type="text/javascript">
  
  $(document).ready(function () {
    dialog = $('#dialog-form').dialog({
      autoOpen: false,
      modal: true
    });    
  });

  function showDetail(detail_url, id){
    $.ajax({
      url: detail_url+id,
      success: function(data) {
        $("#dialog-form .modal-body").html(data);
        dialog.dialog('open');
      }
    });
  }

  function closeDialog(){
    dialog.dialog('close');
  }
</script>

<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
startLoading();
var oTable = $('#tabel_index').DataTable({
      processing: true,
      serverSide: true,
      paging: true,
      searching: false,
      ajax: {
        url : '{{ url("user/data") }}',
        data: function (d) {
          d.username = $('#username').val();
        }
      },
      drawCallback: function( settings ) {
        stopLoading();
      },
      aoColumns: [
                        { mData: 'username' },
                        { mData: 'action' }
                ]
  });

$(function() {
  //oTable.draw();
});

function refreshTable(){
  startLoading();
  oTable.draw();
}

</script>
@endsection


@section('content')

<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                  @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                  {!! Form::text('username', $value = null, $attributes = ['id' => 'username', 'placeholder' => 'Username', 'class' => 'form-control', 'style'=>'width:20%; display: inline; margin-left: 10px; margin-left: 10px']) !!}
                  <a class="btn btn-primary" href="#" onclick="refreshTable()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Filter</a>

                  <table id="tabel_index" class="table">
                  <thead>
                  <tr>
                      <th>Username</th>
                      <th>Action</th>
                  </tr>
                  </thead>
              </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection