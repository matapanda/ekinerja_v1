<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Util;
use App\KinerjaHarian;
use App\KinerjaTahunan;
use App\NilaiSKPBulanan;

class KinerjaBulanan extends Model
{
    //
    protected $table = 'kinerja_bulanan';

    public $timestamps = false;

    protected $fillable   = ['nama', 'pegawai_id', 'kinerja_tahunan_id', 'target_kuantitas', 'satuan_target_kuantitas', 'target_waktu', 'satuan_target_waktu', 'target_kualitas', 'status_target', 'capaian_kuantitas', 'capaian_waktu', 'capaian_kualitas', 'status_capaian', 'skor', 'status', 'capaian_produktifitas'];

    public function get_bulan(){
        return $this->bulan;
    }

    public function kinerja_tahunan()
    {
        return $this->belongsTo('App\KinerjaTahunan', 'kinerja_tahunan_id');
    }

    public function satuan_target_kuantitas()
    {
        return $this->belongsTo('App\SatuanJumlah', 'satuan_target_kuantitas');
    }

    public function satuan_target_waktu()
    {
        return $this->belongsTo('App\SatuanWaktu', 'satuan_target_waktu');
    }

    public function getPersentaseCapaianKuantitas(){
        if($this->target_kuantitas > 0){
            return round((($this->capaian_kuantitas/$this->target_kuantitas)*100),2);
        }
        return 0;
    }

    public function getNilaiWaktu(){
        // Persen waktu = 100 - (realisasi_waktu/target_waktu*100)
        $persen_waktu = 100 - ($this->capaian_waktu/$this->target_waktu*100);
        // Jika persen waktu > 24 maka 76-((((1,76*target_waktu-realisasi_waktu)/target_waktu)*100)-100)
        if($persen_waktu > 24){
            return 76-((((1.76*$this->target_waktu - $this->capaian_waktu) / $this->target_waktu)*100)-100);
        }
        // Sebaliknya maka ((1.76*target_waktu-realisasi_waktu)/target_waktu)*100;
        else{
            return ((1.76*$this->target_waktu-$this->capaian_waktu)/$this->target_waktu)*100;
        }
    }

    public function getNilaiKinerja(){
        return ($this->getPersentaseCapaianKuantitas()+$this->capaian_kualitas+$this->getNilaiWaktu())/3;
    }

    public function changeStatus($status, $capaian_kualitas){
        DB::table('kinerja_harian')->where("kinerja_bulanan_id", $this->id)
            ->update(array('status' => Util::STATUS_SETUJU, 'capaian_kualitas' => $capaian_kualitas));
        $this->hitungKinerja();
    }

    public function hitungKinerja(){
        // Jika kegiatan JABATAN
        if($this->jenis == 0){
            // Hitung skor kinerja bulanan dari kinerja harian yang statusnya SETUJU
            $kinerja_harian = KinerjaHarian::
                where("kinerja_bulanan_id", $this->id)
                ->where("status", Util::STATUS_SETUJU)
                ->get();
            $waktu = "";
            $capaian_waktu = 0;
            $capaian_kuantitas = 0;
            $capaian_kualitas = 0;
            $counter_harian = 0;
            foreach($kinerja_harian as $kr){
                $capaian_kuantitas = $capaian_kuantitas + $kr->capaian_kuantitas;
                $capaian_kualitas = $capaian_kualitas + $kr->capaian_kualitas;
                $counter_harian = $counter_harian + 1;
                if($waktu != $kr->tanggal){
                    $capaian_waktu = $capaian_waktu+1;
                    $waktu = $kr->tanggal;
                }
            }
            $this->capaian_kuantitas = $capaian_kuantitas;
            $this->capaian_waktu = $capaian_waktu;
            $this->capaian_kualitas = $capaian_kualitas/$counter_harian;
            $this->capaian_waktu = $capaian_waktu;
        }
        // Jika kegiatan TAMBAHAN
        else if($this->jenis == 1){
            $stats = $kinerja_harian = KinerjaHarian::
                where("kinerja_bulanan_id", $this->id)
                ->where("status", Util::STATUS_SETUJU)
                ->count();
            // Jika sudah ada kegiatan harian tambahan yang disetujui, set 1 capaian
            if($stats > 0){
                $this->capaian_kuantitas = 0;
            }
            else{
                $this->capaian_kuantitas = 0;
            }
        }
        $this->save();

        // Hitung ulang skor kinerja tahunan
        // Ubah status kegiatan tahunan
        $tahunan = $this->kinerja_tahunan;
        if(!is_null($tahunan)){
            // Get semua kegiatan bulanan yang statusnya disetujui
            $bulanan = KinerjaBulanan::where("kinerja_tahunan_id", $this->kinerja_tahunan_id)->orderBy("bulan","ASC")->get();
            // Hitung akumulasi capaian kuantitas, kualitas dan waktu
            $akumulasi_kuantitas_bulanan = 0;
            $akumulasi_waktu_bulanan = 0;
            $rerata_kualitas_bulanan = 0;
            $counter_bulanan = 0;
            $id_bulan = 0;
            foreach($bulanan as $kb){
                if($tahunan->satuan_target_kuantitas == $kb->satuan_target_kuantitas){
                    $akumulasi_kuantitas_bulanan = $akumulasi_kuantitas_bulanan+$kb->capaian_kuantitas;
                }
                if($id_bulan != $kb->bulan){
                    $akumulasi_waktu_bulanan = $akumulasi_waktu_bulanan + 1;
                    $id_bulan = $kb->bulan;
                }
                $rerata_kualitas_bulanan = $rerata_kualitas_bulanan + $kb->capaian_kualitas;
                $counter_bulanan = $counter_bulanan+1;
            }
            // Set parameter tahunan
            $tahunan->capaian_kuantitas = $akumulasi_kuantitas_bulanan;
            $tahunan->capaian_waktu = $akumulasi_waktu_bulanan;
            $rerata_kualitas_bulanan = $rerata_kualitas_bulanan/$counter_bulanan;
            $tahunan->capaian_kualitas = $rerata_kualitas_bulanan;
            $tahunan->save();
        }

        // Hitung skor SKP bulanan 
        KinerjaBulanan::hitungNilaiSKPBulanan($this->bulan, $this->tahun, $this->pegawai_id);
    }

    public static function hitungNilaiSKPBulanan($bulan, $tahun, $pegawai_id){
        $dataset = KinerjaBulanan::select('kinerja_bulanan.*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id')
                  ->leftJoin('kinerja_tahunan', 'kinerja_tahunan.id', '=', 'kinerja_bulanan.kinerja_tahunan_id')
                  ->leftJoin('satuan_jumlah', 'kinerja_bulanan.satuan_target_kuantitas', '=', 'satuan_jumlah.id')
                  ->leftJoin('satuan_waktu', 'kinerja_bulanan.satuan_target_waktu', '=', 'satuan_waktu.id')
                  ->where('kinerja_tahunan.tahun', $tahun)
                  ->where('kinerja_bulanan.bulan', $bulan)
                  ->where('kinerja_bulanan.pegawai_id', $pegawai_id);
        
        $nilai_tugas_jabatan = 0;
        $counter = 0;
        $nilai_produktifitas = 0;
        $counterKegiatanTambahan = 0;

        foreach($dataset->get() as $kb){
            $nilai_kinerja = 0;
            // Jika kegiatan jabatan
            if($kb->jenis == 0){
                $nilai_kinerja = $kb->getNilaiKinerja();
                $nilai_tugas_jabatan = $nilai_tugas_jabatan + $nilai_kinerja;
                $counter += 1;
            }
            // Jika kegiatan tambahan
            if($kb->jenis == 1){
                $nilai_kinerja = 1;
                $counterKegiatanTambahan += 1;
            } 
        }

        if($counter > 0){
            $nilai_tugas_jabatan = $nilai_tugas_jabatan/$counter;
            $nilai_tugas_jabatan = round($nilai_tugas_jabatan, 2);
        }
        
        $nilai_tugas_tambahan = 0;
        if($counterKegiatanTambahan >= 1 && $counterKegiatanTambahan <= 3){
            $nilai_tugas_tambahan = 1;
        }
        else if($counterKegiatanTambahan >= 4 && $counterKegiatanTambahan <= 6){
            $nilai_tugas_tambahan = 2;
        }
        else if($counterKegiatanTambahan >= 7){
            $nilai_tugas_tambahan = 3;
        }    
        $nilai_skp = $nilai_tugas_jabatan + $nilai_tugas_tambahan;
        // Insert or update dari tabel nilai_skp_bulanan
        $skp = NilaiSKPBulanan::firstOrNew(['pegawai_id' => $pegawai_id, 'bulan' => $bulan, 'tahun' => $tahun ]);
        $skp->nilai_tugas_jabatan = $nilai_tugas_jabatan;
        $skp->nilai_tugas_tambahan = $nilai_tugas_tambahan;
        $skp->nilai_skp = $nilai_skp;
        $skp->save();
        return $skp;
    }
}
