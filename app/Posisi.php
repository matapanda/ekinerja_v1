<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    //
    protected $table = 'posisi';

    public $timestamps = false;

    protected $primaryKey = 'id_posisi';

    protected $connection = 'db_simpeg';

    public function unitk(){
    	return $this->belongsTo('App\UnitK', 'unitk');
    }

    public function subUnitk(){
    	return $this->belongsTo('App\SubUnitK', 'sub_unitk');
    }

    public function golruangmin(){
    	return $this->belongsTo('App\GolRuang', 'golruang_min');
    }
}
