<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    //
    protected $table = 'jabatan';

    public $timestamps = false;

    protected $primaryKey = 'id_jabatan';

    //public $coba;

    public function pegawai(){
    	return $this->belongsTo('App\IdentitasPegawai', 'id_pegawai');
    }

    public function posisi(){
    	return $this->belongsTo('App\Posisi', 'posisi');
    }

    public function golruang(){
    	return $this->belongsTo('App\GolRuang', 'golruang');
    }
}
