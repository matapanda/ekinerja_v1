<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $connection = 'db_simpeg';

    public $timestamps = false;

    public static $roles = array( 
         1 => array('id' => 'superadmin', 'nama' => 'Super Admin'),
         10 => array('id' => 'admin-skpd', 'nama' => 'Administrator SKPD'),
         4 => array('id' => 'user', 'nama' => 'User')
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getAuthPassword()
    {
        return Hash::make($this->password);
    }

    public function getRememberToken()
    {
        return null;
    }

    public function getRememberTokenName()
    {
        return null;
    }

    public function setRememberToken($value)
    {
      // not supported
    }

    public function hasRole($asked_role){
        
        $role_obj = self::$roles[$this->user_level];
        if($asked_role == $role_obj['id']){
            return true;
        }
        return false;
    }

    public function isAdmin(){
        if($this->hasRole('superadmin') || $this->hasRole('admin-skpd')){
            return true;
        }
        return false;
    }

    public function isAdminSKPD(){
        if($this->hasRole('admin-skpd')){
            return true;
        }
        return false;
    }

    public function isSuperAdmin(){
        if($this->hasRole('superadmin')){
            return true;
        }
        return false;
    }

    public function getNamaUnitSubUnitK(){
        $nama = "";
        $unitk = UnitK::where('id_unitk', $this->unitk_skpd)->first();
        $sub_unitk = SubUnitK::where('id_sub_unitk',$this->sub_unitk_skpd)->first();
        if($sub_unitk){
            $nama = $nama.$sub_unitk->keterangan;
            $nama = $nama.'<br>';
        }
        if($unitk){
            $nama = $nama.$unitk->keterangan;
        }
        return $nama;
    }

}