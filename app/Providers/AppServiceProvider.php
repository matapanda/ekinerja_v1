<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\KinerjaHarian;
use App\Util;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use App\Notifikasi;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('date_valid', function($attribute, $value, $parameters, $validator) {
            $datetime = Carbon::createFromFormat( 'd-m-Y', $value);
            $date_month = $datetime->month;
            $date_year = $datetime->year;
            $start_date = Carbon::create($date_year, $date_month, 1,0,0,0);
            $end_date = Carbon::create($date_year, $date_month, 5, 0, 0, 0)->addMonth();
            $today = Carbon::today();
            
            return $today->between($start_date, $end_date, true);
        });

        Validator::extend('time_valid', function($attribute, $value, $parameters, $validator) {
            $date_str = Input::get("tanggal");
            $id = Input::get("update", 0);
            $date = Carbon::createFromFormat( 'd-m-Y', $date_str);
            $time = Carbon::createFromFormat( 'H:i:s', $value);
            $pegawai_id = Util::getPegawaiId();
            if($id != 0){
                $list_kinerja = KinerjaHarian::where("pegawai_id", $pegawai_id)->where('id', '!=', $id)->whereDate('tanggal',"=",$date->toDateString())->get();
            }
            else{
                $list_kinerja = KinerjaHarian::where("pegawai_id", $pegawai_id)->whereDate('tanggal',"=",$date->toDateString())->get();
            }
            foreach($list_kinerja as $kinerja){
                $start_time = Carbon::createFromFormat( 'H:i:s', $kinerja->waktu_awal);
                $end_time = Carbon::createFromFormat( 'H:i:s', $kinerja->waktu_akhir);
                if($time->between($start_time, $end_time)){
                    return false;
                }
            }
            return true;
        });

        KinerjaTahunan::deleting(function($kinerja){
            $bulanan = KinerjaBulanan::where('kinerja_tahunan_id', $kinerja->id);

            $list_bulanan = $bulanan->get(); 
            foreach( $list_bulanan as $kb){
                KinerjaHarian::where('kinerja_bulanan_id', $kb->id)->delete();    
            }

            $bulanan->delete();

        });

        KinerjaBulanan::deleting(function($kinerja){
            KinerjaHarian::where('kinerja_bulanan_id', $kinerja->id)->delete();
        });

        KinerjaHarian::deleting(function($kinerja){
            if(Storage::disk('upload')->has($kinerja->bukti)){
                Storage::disk('upload')->delete($kinerja->bukti);
            }
        });

        KinerjaHarian::deleted(function($kinerja){
            $atasan = Util::getPegawai()->getAtasanLangsung();
            if($atasan){
              $jumlah_notif = Notifikasi::hitungNotifikasi($atasan);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
