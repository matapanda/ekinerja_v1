<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GolRuang extends Model
{
    //

    protected $table = 'golruang';

    public $timestamps = false;

    protected $primaryKey = 'id_golruang';

    public function golongan(){
    	return $this->belongsTo('App\Golongan', 'golongan');
    }
}
