<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eselon extends Model
{
    //
    protected $table = 'eselon';

    public $timestamps = false;

    protected $primaryKey = 'id_eselon';
}
