<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node as BaumNode;

class KinerjaTahunanBulanan extends BaumNode
{
    //
    protected $table = "kinerja_tahunan_bulanan";
    protected $parentColumn = 'kinerja_tahunan';
    protected $guarded = [];
    public $timestamps = false;
}
