<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\Node;
use Baum\Node as BaumNode;

/*
class Instansi extends Node
{
    //
    protected $table = "instansi";
    public $timestamps = false;
    
}*/

class Instansi extends BaumNode
{
    //
    protected $connection = 'db_ref';
    protected $table = "skpd";
    protected $guarded = [];
    public $timestamps = false;

    public function getFullnameAttribute($value)
    {
        return $this->nama;
    }

    public function getInstansiBawahan(){
        return Instansi::where("pId", $this->id);
    }

    public function getInstansiSKPD(){
        return $this->getInstansiBawahan();
    }

    public function getInstansiInduk($instansi){
        $induk = Instansi::where("id",$this->pId)->first();
        if($induk == NULL){
            $induk = $this;
        }
        return $induk;
    }
    
}

