<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitK extends Model
{
    //
    protected $table = 'unitk';

    public $timestamps = false;

    protected $primaryKey = 'id_unitk';

    protected $connection = 'db_simpeg';
}
