<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class Pegawai extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;

    protected $table = 'biodata';

    protected $connection = 'db_master';

    public $timestamps = false;

    protected $primaryKey = 'pegawai_id';

    protected $role = 0;

    public static $roles = array(
         0 => array('id' => 'user', 'nama' => 'User'),
         1 => array('id' => 'admin-skpd', 'nama' => 'Administrator SKPD'),
         2 => array('id' => 'superadmin', 'nama' => 'Super Admin')
    );

    public function getPegawaiId(){
        return $this->pegawai_id;
    }

    public function instansi()
    {
        return $this->belongsTo('App\Instansi', 'instansi_id');
    }

    public function jabatan()
    {
        // Get jenis jabatan
        $jenis_jabatan = DB::connection("db_master")
                ->table("jenis_jabatan")
                ->where("pegawai_id", $this->pegawai_id)->first();

        if(!$jenis_jabatan){
            return null;
        }

        // Pegawai dengan jabatan struktural
        if($jenis_jabatan->jabatan_id == 1){
            $data = DB::connection("db_master")
                ->table("rwyt_jab_struktural")
                ->select("rwyt_jab_struktural.struktural_id as id_riwayat", "rwyt_jab_struktural.*")
                ->where("pegawai_id", $this->pegawai_id)->where("aktif","Y")->first();
            return array("jenis" => "S", "data" => $data);
        }
        // Pegawai dengan jabatan fungsional
        else if($jenis_jabatan->jabatan_id == 2){
            $data = DB::connection("db_master")
                ->table("rwyt_jab_fungsional")                
                ->select("rwyt_jab_fungsional.fungsional_id as id_riwayat", "rwyt_jab_fungsional.*")
                ->where("pegawai_id", $this->pegawai_id)->where("aktif","Y")->first();
            return array("jenis" => "F", "data" => $data);
        }
        // Pegawai dengan jabatan pelaksana
        else if($jenis_jabatan->jabatan_id == 3){
            $data = DB::connection("db_master")
                ->table("rwyt_jab_pelaksana")
                ->select("rwyt_jab_pelaksana.pelaksana_id as id_riwayat", "rwyt_jab_pelaksana.*")
                ->where("pegawai_id", $this->pegawai_id)->where("aktif","Y")->first();
            return array("jenis" => "P", "data" => $data);
        }
    }

    public function getAuthPassword()
    {
        return Hash::make($this->password);
    }

    public function getRememberToken()
    {
        return null;
    }

    public function getRememberTokenName()
    {
        return null;
    }

    public function setRememberToken($value)
    {
      // not supported
    }

    public function setPasswordAttribute($value) 
    { 
        if (!empty($value)) 
            $this->attributes['password'] = Hash::make($value);
    }

    public function getPasswordAttribute($value) 
    { 
        return strtolower($value);
    }

    public function hasRole($asked_role){
        $role_obj = self::$roles[$this->role];
        if($asked_role == $role_obj['id']){
            return true;
        }
        return false;
    }

    public static function getNamaRole($role_key){
        return self::$roles[$role_key]['nama'];
    }

    public static function getRoleOptions(){
        $arr_options = array();
        foreach (self::$roles as $key => $value) {
            $arr_options[$key] = $value['nama'];
        }
        return $arr_options;
    }

    public function getCalonAtasanLangsung(){
        return $this->getCalonAtasan();
    }

    public function getCalonAtasan(){
        $skpd_id = $this->skpd_id;
        $jml_struktural = DB::connection("db_master")->table("rwyt_jab_struktural")->where("pegawai_id","=",$this->pegawai_id)->where("aktif","Y")->count();
        // Jika saya adalah pejabat struktural, maka atasan saya adalah pejabat struktural pada unit di atas saya
        if($jml_struktural > 0){
            $induk_skpd = DB::connection("db_ref")->table("skpd")->where("id",$this->skpd_id)->first();
            if($induk_skpd === NULL){
                return NULL;
            }
            else{
                $skpd_id = $induk_skpd->pId;
            }
        }
        // Else, jika saya adalah pegawai fungsional, maka atasan adalah pejabat struktural di unit yang sama
        else{
            
        }
        $atasan = DB::connection("db_master")->table("biodata")
            ->join("rwyt_jab_struktural as rwyt","biodata.pegawai_id","=","rwyt.pegawai_id")
            ->where("skpd_id",$skpd_id)
            ->where("rwyt.aktif","Y")
            ->get();
        return $atasan;
    }

    public function getCalonAtasanLangsungInstance(){
        return $this->getCalonAtasanLangsung();
    }

    public function getBawahanLangsung(){
        $skpd_bawahan = DB::connection("db_ref")->table("skpd")->where("pId", $this->skpd_id)->get();
        // Jika saya adalah pejabat struktural dari unit yang punya unit bawahan 
        if(sizeof($skpd_bawahan) > 0){
            $arr_skpd_id = array();
            foreach($skpd_bawahan as $skpd){
                array_push($arr_skpd_id, $skpd->id);
            }
            return Pegawai::join("rwyt_jab_struktural as rwyt","biodata.pegawai_id","=","rwyt.pegawai_id")->where("rwyt.aktif","Y")->whereIn('skpd_id', $arr_skpd_id)->get();
        }
        // Else, jika saya adalah pejabat struktural dari unit yang TIDAK punya unit bawahan
        else{
            return Pegawai::where("skpd_id", $this->skpd_id)->where('pegawai_id', '!=', $this->pegawai_id)->get();
        }  
    }

    public function getAtasanLangsung(){
        $atasan =  AtasanBawahan::where('bawahan_id', "=", $this->pegawai_id)->first();
        if($atasan){
            return Pegawai::where("pegawai_id", $atasan->atasan_id)->first();
        }
        return NULL;
    }

    public function getAtasanLangsungId(){
        $atasan =  AtasanBawahan::where('bawahan_id', "=", $this->pegawai_id)->first();
        if($atasan){
            //return Pegawai::where("pegawai_id", $atasan->atasan_id)->first()->pegawai_id;
            return $atasan->atasan_id;
        }
        return NULL;
    }

    public function getBawahan(){
        return AtasanBawahan::
            join("master.biodata as biodata", "biodata.pegawai_id", "=", "atasan_bawahan.bawahan_id")
            ->where('atasan_id', "=", $this->pegawai_id);
    }

    public function isAdmin(){
        return false;
    }

    public function hasBawahan(){
        return true;
    }

    public function isAtasan($bawahan_id){
        return AtasanBawahan::where("atasan_id", "=", $this->pegawai_id)
                ->where("bawahan_id", "=", $bawahan_id)->first();
    }

    public function getBobotJabatan(){
        return 0;
    }

    public function getNamaJabatan()
    {
        // Get jenis jabatan
        $jenis_jabatan = DB::connection("db_master")
                ->table("jenis_jabatan")
                ->where("pegawai_id", $this->pegawai_id)->first();

        if(!$jenis_jabatan){
            return "";
        }

        // Pegawai dengan jabatan struktural
        if($jenis_jabatan->jabatan_id == 1){
            $data_jabatan = DB::connection("db_master")
                ->table("rwyt_jab_struktural")
                ->select("*")
                ->where("pegawai_id", $this->pegawai_id)->where("aktif","Y")->first();
            $unit_kerja = $data_jabatan->unit_kerja;
            $data_jabatan = DB::connection("db_ref")
                ->table("ref_jabatan_struktural")
                ->select("*")
                ->where("jab_struktural_id", $data_jabatan->jab_struktural_id)->first();
            $nama_jabatan = $data_jabatan->jab_struktural;
            return $nama_jabatan." - ".$unit_kerja;
        }
        // Pegawai dengan jabatan fungsional
        else if($jenis_jabatan->jabatan_id == 2){
            return "-";
        }
    }

    public function getDummy(){
        return "Hello";
    }
    
}
