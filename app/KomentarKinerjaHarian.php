<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarKinerjaHarian extends Model
{
    protected $table = 'komentar_kinerja_harian';

    public $timestamps = false;
}
