<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AtasanBawahan;
use App\Eselon;
use App\Posisi;
use App\JabatanTerakhir;
use App\GolRuang;
use App\GajiGolongan;
use App\UnitK;
use App\SubUnitK;
use App\BobotJabatan;
use App\Notifikasi;
use Log;
use DB;

class IdentitasPegawai extends Model
{
    //
    protected $table = 'identitas_pegawai';

    public $timestamps = false;

    protected $primaryKey = 'id_pegawai';

    protected $connection = 'db_simpeg';

    public function jabatan(){
    	return $this->hasMany('App\Jabatan');
    }

    public function jabatan_terakhir(){
        return $this->hasOne('App\JabatanTerakhir');
    }

    public function getAtasanInstansi(){

    }

    public function getBawahanInstansi(){

    }

    public function getCalonAtasan(){
        $id_unitk = $this->getUnitK()->id_unitk;

        $id_eselon_terakhir = $this->getEselonIdTerakhir();        

        /*
        $list_calon_atasan = 
        //IdentitasPegawai::select('identitas_pegawai.*','posisi.keterangan as keterangan_posisi', 'unitk.keterangan as keterangan_unitk')
            IdentitasPegawai::leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
            ->leftJoin('posisi', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')
            ->leftJoin('unitk', 'posisi.unitk', '=', 'unitk.id_unitk')
            ->leftJoin('identitas_kepegawaian', 'identitas_kepegawaian.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
            ->leftJoin('status_pegawai', 'identitas_kepegawaian.status_pegawai', '=', 'status_pegawai.id_status_pegawai')            
            ->where('posisi.unitk',$id_unitk)
            ->where('status_pegawai.keterangan','PNS')
            ->where('posisi.eselon_jabatan', '<', 8);
        */
        $list_calon_atasan = 
                IdentitasPegawai::Join('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
                ->Join('posisi', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')
				//LEFT JOIN pangkat_terakhir ON identitas_pegawai.id_pegawai = pangkat_terakhir.id_pegawai
				->Join('pangkat_terakhir', 'identitas_pegawai.id_pegawai', '=', 'pangkat_terakhir.id_pegawai')
                 ->Join('identitas_kepegawaian', 'identitas_pegawai.id_pegawai', '=', 'identitas_kepegawaian.id_pegawai')
				//->where('posisi.unitk',$id_unitk)
				->whereIn('identitas_kepegawaian.keadaan_pegawai', array(1,3,4,6,10))
				->whereIn('identitas_kepegawaian.status_pegawai',array(1,2))
				->orderby('identitas_pegawai.nama', 'asc');
                //->leftJoin('unitk', 'posisi.unitk', '=', 'unitk.id_unitk')
                
        
        if($id_eselon_terakhir != 0){
            $list_calon_atasan = $list_calon_atasan->where('posisi.eselon_jabatan', '<=',$id_eselon_terakhir);
        }
        $data = $list_calon_atasan->get();
        return $data;
    }

    public function getAtasanLangsung(){
        $atasan = AtasanBawahan::where('bawahan_id', $this->id_pegawai)->first();
        if($atasan){
            return IdentitasPegawai::select('identitas_pegawai.*','posisi.keterangan as keterangan_posisi', 'unitk.keterangan as keterangan_unitk')
                ->leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
                ->leftJoin('posisi', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')
                ->leftJoin('unitk', 'posisi.unitk', '=', 'unitk.id_unitk')
                ->where('identitas_pegawai.id_pegawai', $atasan->atasan_id)
                ->first();
        }
        else{
            return null;
        }
    }

    public function getAtasanLangsungInstance(){
        $atasan = AtasanBawahan::where('bawahan_id', $this->id_pegawai)->first();
        if($atasan){
            return IdentitasPegawai::
                where('id_pegawai', $atasan->atasan_id)
                ->first();
        }
        else{
            return null;
        }
    }

    public function getAtasanLangsungId(){
        $atasan = AtasanBawahan::where('bawahan_id', $this->id_pegawai)->first();
        if($atasan){
            return $atasan->atasan_id;
        }
        else{
            return null;
        }
    }

    public function getBawahanLangsung($with_posisi=true){
        $list_id_bawahan = AtasanBawahan::where('atasan_id',$this->id_pegawai)->lists('bawahan_id')->toArray();
        if($with_posisi){
            $list_bawahan = IdentitasPegawai::select('identitas_pegawai.*','posisi.keterangan as keterangan_posisi', 'unitk.keterangan as keterangan_unitk')
            ->leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
            ->leftJoin('posisi', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')
            ->leftJoin('unitk', 'posisi.unitk', '=', 'unitk.id_unitk')
            ->whereIn('identitas_pegawai.id_pegawai', $list_id_bawahan);
        }
        else{
            $list_bawahan = IdentitasPegawai::select('identitas_pegawai.*')
            ->whereIn('identitas_pegawai.id_pegawai', $list_id_bawahan);
        }
        return $list_bawahan;
    }

    public function getBawahan(){
        $list_id_bawahan = AtasanBawahan::where('atasan_id',$this->id_pegawai)->lists('bawahan_id')->toArray();
        $list_bawahan = IdentitasPegawai::select('identitas_pegawai.*')
            ->whereIn('identitas_pegawai.id_pegawai', $list_id_bawahan);
        return $list_bawahan;
    }

    public function getUnitK(){
        return UnitK::select('unitk.*')
            ->join('posisi', 'unitk.id_unitk', '=', 'posisi.unitk')
            ->join('jabatan_terakhir', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')            
            ->where('jabatan_terakhir.id_pegawai', $this->id_pegawai)
            ->first();
    }

    public function getEselon(){
        return Eselon::select('eselon.*')
            ->join('jabatan_terakhir', 'eselon.id_eselon', '=', 'jabatan_terakhir.eselon')        
            ->where('jabatan_terakhir.id_pegawai', $this->id_pegawai)
            ->first();
    }

    public function getSemuaPosisi(){
        return Posisi::select('posisi.*')
                        ->join('jabatan', 'jabatan.posisi', '=', 'posisi.id_posisi')
                        ->where('jabatan.id_pegawai', $this->id_pegawai);
    }

    public function getPosisiIdTerakhir(){
        return JabatanTerakhir::where('id_pegawai', $this->id_pegawai)->first()->posisi;
    }

    public function getEselonIdTerakhir(){
        return JabatanTerakhir::where('jabatan_terakhir.id_pegawai', $this->id_pegawai)
            ->first()->eselon;
    }

    public function getGolruangIdTerakhir(){
        return DB::table('pangkat_terakhir')->where('pangkat_terakhir.id_pegawai', $this->id_pegawai)->first()->golruang;
    }

    public function getGolruangTerakhir(){
        return GolRuang::join('pangkat_terakhir', 'pangkat_terakhir.golruang','=','golruang.id_golruang')
            ->where('pangkat_terakhir.id_pegawai', $this->id_pegawai)
            ->first();
    }

    public function getPangkatTerakhir(){
        return DB::table('pangkat_terakhir')->where('pangkat_terakhir.id_pegawai', $this->id_pegawai)->first();
    }

    public function getPegawaiId(){
        return $this->id_pegawai;
    }

    public function isAtasan($pegawai_id){
        $atasan = AtasanBawahan::where('atasan_id', $this->id_pegawai)->where('bawahan_id', $pegawai_id)->get();
        if($atasan){
            return true;
        }
        else{
            return false;
        }
    }

    public function getBobotJabatan(){
        $bobot_jabatan = DB::table('bobot_jabatans')
            ->select('bobot_jabatans.nilai as nilai')
            ->leftJoin('klasifikasi_bobot_jabatan_pegawais', 'klasifikasi_bobot_jabatan_pegawais.id_bobot_jabatan', '=', 'bobot_jabatans.id')
            ->where('klasifikasi_bobot_jabatan_pegawais.id_pegawai', $this->id_pegawai)->first();
        
        if(isset($bobot_jabatan->nilai)){
            return $bobot_jabatan->nilai;
        }
        else{
            return 0;
        }
    }

    public function getPosisiTerakhir(){
        return Posisi::select('posisi.*')
            ->join('jabatan_terakhir', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')            
            ->where('jabatan_terakhir.id_pegawai', $this->id_pegawai)
            ->first();   
    }

    public function getNamaJabatan()
    {
        $posisi = $this->getPosisiTerakhir();
        if($posisi){
            return $posisi->keterangan;
        }
        else{
            return "-";
        }
    }

    public function getNamaUnitSubUnitK(){
        $nama = "";
        $posisi_terakhir = $this->getPosisiTerakhir();
        $unitk = UnitK::where('id_unitk', $posisi_terakhir->unitk)->first();
        $sub_unitk = SubUnitK::where('id_sub_unitk',$posisi_terakhir->sub_unitk)->first();
        if($sub_unitk){
            $nama = $nama.$sub_unitk->keterangan;
            $nama = $nama.'<br>';
        }
        if($unitk){
            $nama = $nama.$unitk->keterangan;
        }
        return $nama;
    }

    public function hasBawahan(){
        $jml_bawahan = AtasanBawahan::where('atasan_id', $this->getPegawaiId())->count();
        if($jml_bawahan > 0){
            return true;
        }
        return false;
    }

    public function hitungNotifikasiSebagaiAtasan(){
        return Notifikasi::hitungNotifikasiSebagaiAtasan($this->id_pegawai);
    }

    public function hitungNotifikasiSebagaiPegawai(){
        return Notifikasi::hitungNotifikasiSebagaiPegawai($this->id_pegawai);
    }

    public function tambahNotifikasiSebagaiAtasan($jumlah=1){
        return Notifikasi::tambahNotifikasiSebagaiAtasan($this->id_pegawai, $jumlah);
    }

    public function kurangiNotifikasiSebagaiAtasan($jumlah=1){
        return Notifikasi::kurangiNotifikasiSebagaiAtasan($this->id_pegawai, $jumlah);
    }
}
