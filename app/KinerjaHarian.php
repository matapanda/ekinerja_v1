<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KinerjaHarian extends Model
{
    //
    protected $table = 'kinerja_harian';

    public $timestamps = false;

    protected $dates = ['tanggal'];

    public function satuan_target_kuantitas()
    {
        return $this->belongsTo('App\SatuanJumlah', 'satuan_target_kuantitas');
    }

    public function satuan_kuantitas()
    {
        return $this->belongsTo('App\SatuanJumlah', 'satuan_target_kuantitas');
    }

    public function kinerja_bulanan()
    {
        return $this->belongsTo('App\KinerjaBulanan', 'kinerja_bulanan_id');
    }

    public function identitas_pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'pegawai_id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\IdentitasPegawai', 'pegawai_id');
    }
}
