<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SatuanJumlahController extends SuperAdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model = '\\App\\SatuanJumlah';

    protected $page_title = 'Satuan Jumlah';

    protected function define_columns($grid){
       $grid->add('nama','Nama', true); 
       $grid->edit('satuan_jumlah/form', 'Edit','show|modify|delete');     
       $grid->link('satuan_jumlah/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','asc');
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama Satuan Jumlah', 'text')->rule('required');
      $edit->link("satuan_jumlah","Kembali")->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('satuan_jumlah')->with('message', 'Data berhasil disimpan');
      });     
    }

    protected function define_filters($filter){
       $filter->add('nama','Nama', 'text');
       $filter->submit('search');
       $filter->reset('reset');
    }
}
