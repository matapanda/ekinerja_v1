<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\NilaiSKPBulanan;
use DB;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;
use App\Util;
use App\KinerjaBulanan;

class AdminNilaiSKPBulananController extends AdminController
{
    protected $model = '\\App\\NilaiSKPBulanan';

    protected $page_title = 'Nilai SKP Bulanan';

    protected $form_view = 'form';

    public function getIndex()
    {
        $index_view = 'admin.index_nilai_skp_bulanan';

        $list_bulan = Util::get_all_bulan();

        $selected_bulan = Carbon::now()->month;
        if(Input::get('bulan')){
            $selected_bulan = Input::get('bulan');        
        }

        $selected_tahun = Carbon::now()->year;
        if(Input::get('tahun')){
            $selected_tahun = Input::get('bulan');        
        }

        return view($index_view)
                ->with('list_bulan',$list_bulan)
                ->with('page_title',$this->page_title)
                ->with('selected_bulan',$selected_bulan)
                ->with('selected_tahun',$selected_tahun)
                ->with('base_url',action(class_basename($this)."@getIndex"));
    }

    public function getData(){
        $query = $this->get_dataset();
        $datatables = Datatables::of($query)
          ->addColumn('action', function($skp) {
                $new_link = '<a class="btn btn-info btn-sm" title="Hitung" href="#" onclick=hitungUlang('.$skp->pegawai_id.')> Hitung </a> ';
                return $new_link;
              }
          );
        
          if($bulan = $datatables->request->get('bulan')) {
            $datatables->where('bulan', $bulan);
          }
    
          if($tahun = $datatables->request->get('tahun')) {
            $datatables->where('tahun', $tahun);
          }

          if($nama = $datatables->request->get('nama')) {
            $datatables->where('simpeg.nama', 'like', '%'.$nama.'%');
          }
    
          return $datatables->make(true);
      }
    
    public function postHitung($tahun, $bulan, $pegawai_id){
        return KinerjaBulanan::hitungNilaiSKPBulanan($bulan, $tahun, $pegawai_id);
    } 

    public function postHitungsemua($tahun, $bulan){
        $kinerja_bulanan = KinerjaBulanan::distinct()
            ->select('pegawai_id')
            ->where('tahun', $tahun)
            ->where('bulan', $bulan)
            ->groupBy('pegawai_id')
            ->get();
        foreach($kinerja_bulanan as $kb){
            KinerjaBulanan::hitungNilaiSKPBulanan($bulan, $tahun, $kb->pegawai_id);
        }
        return "Sukses";
    }

    protected function get_dataset(){
        $dataset = NilaiSKPBulanan::leftJoin("simpeg.identitas_pegawai as identitas_pegawai", "identitas_pegawai.id_pegawai", "=", "pegawai_id");
        return $dataset;
    }

}
