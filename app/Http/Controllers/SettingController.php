<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Setting;

class SettingController extends SuperAdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model = '\\App\\Setting';

    protected $page_title = 'Setting';

        protected $form_view = 'form';


    public function getIndex(){

    }

    public function anyForm(Request $request){
        $setting = Setting::firstOrCreate(['id' => 1]);
        $edit = \DataForm::source($setting);
        $edit->add('tunjangan_dasar', 'Tunjangan Dasar','text');
        $edit->link($this->getRedirectUrl(),trans('rapyd::rapyd.back'))->back();
        $edit->saved(function () use ($edit) {           
            return Redirect::to($this->getRedirectUrl())->with('message' , 'Data atasan berhasil disimpan');            
        }); 
        $page_activity = 'Ubah';
        $edit->submit('Simpan','BR');
        $result = $edit->view($this->form_view,compact('edit'))
              ->with('page_title',$this->page_title)
              ->with('page_activity',$page_activity)
              ->with('base_url',action(class_basename($this)."@anyForm"));     
        return $result;
    }

    protected function getRedirectUrl(){
      return 'setting';
    }
}
