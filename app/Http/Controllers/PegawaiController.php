<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Req;
use App\Instansi;
use App\Jabatan;
use App\Pegawai;

class PegawaiController extends AdminController
{

    public function __construct(Req $request)
    {   
        if(Auth::user()){     
          if(! (Auth::user()->hasRole('superadmin') || Auth::user()->hasRole('admin-skpd')) ){
            return Redirect::to('/')->with('message', 'Halaman tidak boleh diakses')->send();
          }
        }
        else{
          return Redirect::to('/')->with('message', 'Halaman tidak boleh diakses')->send();
        }
    }

    protected $model = '\\App\\Pegawai';

    protected $page_title = 'Pegawai';

    protected function define_columns($grid){
       $grid->add('nip','NIP', true);
       $grid->add('nama','Nama', true); 
       $grid->add('{{ $jabatan->nama }}','Jabatan'); 
       $grid->add('{{ $instansi->nama }}','Instansi');
       $grid->add('role','Wewenang')->cell( function( $value, $row) {            return Pegawai::getNamaRole($row->role);        });
       $grid->edit('pegawai/form', 'Edit','show|modify|delete');
       $grid->row(function ($row) {
         $new_link = '<a class="" title="Change Password" href="pegawai/changepassword/'.$row->data->id.'"><span class="glyphicon glyphicon-user"> </span></a>';
         $row->cell('_edit')->value = $row->cell('_edit')->value.$new_link;
       });
       $grid->link('pegawai/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','asc');
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama', 'text')->rule('required|max:255');
      $edit->add('nip','NIP', 'text')->rule('required|max:20');
      $edit->add('pendidikan','Pendidikan', 'text')->rule('required');
      $edit->add('alamat','Alamat', 'text');
      
      if($edit->status == 'create'){
        $edit->add('username','Username', 'text')->rule('required|max:255|unique:pegawai');
        $edit->add('password','Password', 'password')->rule('required|min:6');
      }
      else{
        $edit->add('username','Username', 'text')->mode('readonly');
      }
      $edit->add('email','Email', 'text')->rule('required|email|max:255');
      if(Auth::user()->hasRole('admin-skpd')){
        $instansi_arr = Auth::user()->instansi->getRootNestedList('nama', null, '---');
      }
      else{
        $instansi_arr = Instansi::getNestedList('nama', null, '---');  
      }      
      $edit->add('instansi','Instansi','select')->options($instansi_arr);
      $edit->add('jabatan','Jabatan','select')->options(Jabatan::lists('nama', 'id')->all());

      if(Input::hasFile('foto')){
        $ekstensi = Input::file('foto')->getClientOriginalExtension();
      }
      else{
        $ekstensi = NULL;
      }

      $edit->add('foto','Foto', 'image')->move('upload','{{ $nip }}.'.$ekstensi)->fit(160, 160)->preview(160,160);
      $edit->add('role','Wewenang','select')->options(Pegawai::getRoleOptions());   
      $edit->link("pegawai","Kembali")->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('pegawai')->with('message', 'Success');
      });     
    }
    public function anyChangepassword($id){
      $edit = \DataForm::source($this->get_dataset()->find($id));
      $edit->add('password','New Password', 'password')->rule('confirmed|required|min:6');
      $edit->add('password_confirmation','Confirm New Password', 'password');
      $edit->submit('Save','BR');
      $edit->link("pegawai","Kembali")->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Password changed");
        return Redirect::to('pegawai')->with('message', 'Success');
      });

      $form_view = (isset($this->form_view) ? 
            $this->form_view : 'form');

      $result = $edit->view($form_view, compact('edit'))
        ->with('page_title','Pegawai')
        ->with('base_url',action(class_basename($this)."@getIndex"))
        ->with('page_activity','Ubah Password');     
      return $result;
    }

    protected function define_filters($filter){
       $filter->add('nama','Nama', 'text');
       $filter->submit('search');
       $filter->reset('reset');
    }

    protected function get_dataset(){
        $obj = new $this->model;
        $obj = $obj::with('jabatan','instansi');
        if(Auth::user()->hasRole('admin-skpd')){
          $instansi = Auth::user()->instansi;
          $instansi_bawahan = $instansi->getInstansiSKPD();
          return $obj->whereIn('instansi_id', $instansi_bawahan);
        }
        return $obj;
    }    

    public function getTest(){
      $instansi_induk = Auth::user()->instansi->getRootNestedList('nama', null, '---');
      dd( $instansi_induk);
    }
}
