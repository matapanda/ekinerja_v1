<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response as FacadeResponse;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\SatuanJumlah;
use App\SatuanWaktu;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use App\Pegawai;
use Zofe\Rapyd\DataGrid;
use App\Util;

class KinerjaTahunanBawahanController extends KinerjaTahunanController
{
   protected function define_fields($edit){   
      if($edit->status == 'show'){
         $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
         $edit->add('deskripsi','Deskripsi Kegiatan', 'redactor');
         $edit->add('tahun','Tahun', 'text')->mode('readonly');

         $edit->add('target_kuantitas','Target Kuantitas', 'text')->rule('required');

         $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());

         $edit->add('target_waktu','Target Waktu', 'text')->rule('required');

         $edit->add('satuan_target_waktu','<br/>','select')->options(SatuanWaktu::lists('nama', 'id')->all());

         $edit->add('target_kualitas','Target Kualitas (%)', 'text')->rule('required');

         
         $edit->text('capaian_kuantitas','Capaian Kuantitas')->mode('readonly');
         $edit->text('capaian_waktu','Capaian Waktu')->mode('readonly');
         $edit->text('capaian_kualitas','Capaian Kualitas (%)')->mode('readonly');
      }
      else{
         $edit->add('status','Status Kegiatan', 'hidden');
      } 
     
    }

    public function getTahunanbulanan(Request $request, $pegawai_id){
      $pegawai = Util::getPegawaiWithPosisi($pegawai_id);
      if(!$pegawai){
        return FacadeResponse::make('Pegawai tidak ditemukan', 404);
      }
      if(!$this->pegawai->isAtasan($pegawai_id)){
        return FacadeResponse::make('Halaman tidak berhak diakses', 401);
      }
      // Kinerja tahunan/bulanan      
      $tahun = $request->get('tahun');
      $kinerja_tahunan = KinerjaTahunan::where('pegawai_id', $pegawai_id);
      $kinerja_bulanan = KinerjaBulanan::select('*', 'kinerja_bulanan.nama as nama_bulanan', 'kinerja_tahunan.nama as nama_tahunan', 'kinerja_bulanan.id as id_bulanan', 'kinerja_tahunan.id as id_tahunan', 'kinerja_bulanan.status as status_bulanan', 'kinerja_tahunan.status as status_tahunan')->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')->where('kinerja_tahunan.pegawai_id', $pegawai_id);
      if($tahun){
        $kinerja_tahunan = $kinerja_tahunan->where('tahun', $tahun);
        $kinerja_bulanan = $kinerja_bulanan->where('kinerja_tahunan.tahun', $tahun); 
      }
      $kinerja_tahunan = $kinerja_tahunan->get();
      $kinerja_bulanan = $kinerja_bulanan->get();

      $arr_kinerja = array();
      foreach($kinerja_tahunan as $kt){
        $arr_kinerja[] = array(
            "id" => $kt->id,
            "nama" => '<b><a href="#" onclick=showDetail("/kinerja_tahunan_bawahan/form?show=",'.$kt->id.')>'.$kt->nama.'</a></b>',
            "tahun_bulan" => "<b>".$kt->tahun."</b>"
            /*"status" => $this->getStatusSign($kt->status, 'tahunan_'.$kt->id),
            "action" => '
                            <a class="btn btn-success btn-xs" href="#" onclick=accept("/kinerja_tahunan_bawahan/form?update=",'.$kt->id.',"tahunan_")>Setujui</a>
                            <a class="btn btn-danger btn-xs" href="#" onclick=reject("/kinerja_tahunan_bawahan/form?update=",'.$kt->id.',"tahunan_")>Tolak</a>'*/
          );
        foreach($kinerja_bulanan as $kb){
          if($kb->kinerja_tahunan_id == $kt->id){
            $arr_kinerja[] = array(
            "id" => $kb->id_bulanan,
            "nama" => '<p style="padding-left : 15px"><a href="#" onclick=showDetail("/kinerja_bulanan_bawahan/form?show=",'.$kb->id_bulanan.')>'.$kb->nama_bulanan.'</a></p>',
            "tahun_bulan" => Util::get_bulan($kb->bulan)
            /*"status" => $this->getStatusSign($kb->status_bulanan, 'bulanan_'.$kb->id_bulanan),
            "action" => '
                            <a class="btn btn-success btn-xs" href="#" onclick=accept("/kinerja_bulanan_bawahan/form?update=",'.$kb->id_bulanan.',"bulanan_")>Setujui</a>
                            <a class="btn btn-danger btn-xs" href="#" onclick=reject("/kinerja_bulanan_bawahan/form?update=",'.$kb->id_bulanan.',"bulanan_")>Tolak</a>'*/
            );
          }
        }
      }

      $grid = \DataGrid::source($arr_kinerja);
      $grid->paginate($this->pagination);
      $grid->add('nama','Nama Kegiatan');
      $grid->add('tahun_bulan','Tahun/Bulan');
      //$grid->add('status','Status');
      //$grid->add('action','Action');

      return View::make('ajax.tabel_tahunan_bulanan')
                        ->with('grid' , $grid);    
   }

   protected function checkFormPermission($edit){
    if($edit->status == 'modify' || $edit->status == 'delete' ){
      return false;
    }
    if($edit->status == 'show'){
      $kinerja = KinerjaTahunan::where('id', $edit->model->id)->first();
      if(!$kinerja){
        return false;
      }
      else{
        if(!$this->pegawai->isAtasan($kinerja->pegawai_id)){
          return false;
        }
      }
    }    
    return true;
  }

}
