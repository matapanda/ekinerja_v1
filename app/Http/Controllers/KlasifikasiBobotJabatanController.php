<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Util;
use DB;
use Zofe\Rapyd\DataForm;
use App\IdentitasPegawai;
use App\KlasifikasiBobotJabatanPegawai;
use App\BobotJabatan;
use Auth;
use Yajra\Datatables\Facades\Datatables;

class KlasifikasiBobotJabatanController extends AdminController
{
    protected $model = '\\App\\KlasifikasiBobotJabatanPegawai';

    protected $page_title = 'Klasifikasi Bobot Jabatan Pegawai';

    protected $form_view = 'form';

    public function getIndex()
    {
        $index_view = 'admin.index_klasifikasi_bobot_jabatan';
        return view($index_view)
                ->with('list_pegawai', $this->get_dataset()->get())
                ->with('page_title',$this->page_title)
                ->with('base_url',action(class_basename($this)."@getIndex"));
    }

    public function getData(){
        $dataset = $this->get_dataset();
        return \Datatables::of($dataset)->make(true);
    }

   
    public function anyForm(Request $request){
        if(Input::has('pegawai_id')){
            $pegawai_id = Input::get("pegawai_id");
            $pegawai = Pegawai::where('pegawai_id',$pegawai_id)->first();
            if(!$pegawai){
                 return Redirect::to($this->getRedirectUrl())->with('message' , 'Pegawai tidak ditemukan');
            }
        }
        else{
            $pegawai = $this->pegawai;
            $pegawai_id = $pegawai->getPegawaiId();
        }
        $atasan_bawahan = KlasifikasiBobotJabatanPegawai::firstOrCreate(['id_pegawai' => $pegawai_id]);
        $edit = \DataForm::source($atasan_bawahan);
        $edit->add('id_pegawai', 'Pegawai','select')->options(array($pegawai_id => $pegawai->nama))->mode('readonly');
        $edit->add('id_bobot_jabatan', 'Bobot Jabatan','select')->options(BobotJabatan::all()->lists('nama', 'id'));
        $edit->link($this->getRedirectUrl(),trans('rapyd::rapyd.back'))->back();
        $edit->saved(function () use ($edit) {        
            if(Input::has('pegawai_id')){                
                return Redirect::to($this->getRedirectUrl())->with('message' , 'Data atasan berhasil disimpan');
            }
            else{
                return Redirect::to('/')->with('message' , 'Data atasan berhasil disimpan');
            }
        }); 
        $page_activity = 'Ubah';
        $edit->submit('Simpan','BR');
        $result = $edit->view($this->form_view,compact('edit'))
              ->with('page_title',$this->page_title)
              ->with('page_activity',$page_activity)
              ->with('base_url',action(class_basename($this)."@getIndex"));     
        return $result;
    }

    protected function get_dataset(){
        $dataset = DB::
                    connection('db_simpeg')
                    ->table('identitas_pegawai')
                    ->select('identitas_pegawai.id_pegawai AS id_pegawai','identitas_pegawai.nama as nama_pegawai', 'bobot_jabatans.nama as nama_klasifikasi', 'bobot_jabatans.nilai as nilai','identitas_pegawai.nip_baru as nip_baru')
                    ->leftJoin('ekinerja.klasifikasi_bobot_jabatan_pegawais', 'klasifikasi_bobot_jabatan_pegawais.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
                    ->leftJoin('ekinerja.bobot_jabatans', 'bobot_jabatans.id', '=', 'klasifikasi_bobot_jabatan_pegawais.id_bobot_jabatan');
        if(Auth::user()->isAdmin()){            
            $dataset = $dataset->leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'identitas_pegawai.id_pegawai')->leftJoin('posisi', 'jabatan_terakhir.posisi', '=', 'posisi.id_posisi');
            if($this->user->sub_unitk_skpd){
                $dataset = $dataset->where('posisi.sub_unitk', $this->user->sub_unitk_skpd);
            }
            if($this->user->unitk_skpd){
                $dataset = $dataset->where('posisi.unitk', $this->user->unitk_skpd);   
            }
            
        }
        return $dataset;
    }

    protected function define_filters($filter){
        /*
       $filter->add('bawahan.nama','Nama Pegawai', 'text');
       $filter->submit('search');
       $filter->reset('reset');
       */
    }

    protected function getRedirectUrl(){
      return 'klasifikasi_bobot_jabatan';
    }
}
