<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notifikasi;
use App\Util;
use Illuminate\Support\Facades\Auth;

class NotifikasiController extends Controller
{

    public function getAtasan(){        
        return Notifikasi::getNotifikasiSebagaiAtasan(Util::getPegawaiId());
    }

    public function getPegawai(){        
        return Notifikasi::getNotifikasiSebagaiPegawai(Util::getPegawaiId());
    }

    public function getHitung(){
        $notifikasi = Notifikasi::all();
        foreach($notifikasi as $notif){
            Notifikasi::hitungNotifikasiSebagaiAtasan($notif->pegawai_id);
            Notifikasi::hitungNotifikasiSebagaiPegawai($notif->pegawai_id);
        }
        return "Success";
    }

    public function getHitungid($pegawai_id){
        Notifikasi::hitungNotifikasiSebagaiPegawai($pegawai_id);
        return Notifikasi::hitungNotifikasiSebagaiAtasan($pegawai_id);
    }

    public function getKurangi($pegawai_id){
        return Notifikasi::kurangiNotifikasiSebagaiAtasan($pegawai_id,1);
    }
}
