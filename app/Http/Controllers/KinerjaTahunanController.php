<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\SatuanJumlah;
use App\SatuanWaktu;
use App\KinerjaTahunan;
use App\Util;

class KinerjaTahunanController extends KinerjaController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $model = '\\App\\KinerjaTahunan';

    protected $form_view = 'form_kinerja';

    protected $index_view = 'index_kinerja_tahunan';

    protected $page_title = 'Kegiatan Tahunan';

    protected $page_title_tambahan = 'Kegiatan Tambahan';

    protected $form_view_tambahan = 'form_kinerja_tahunan_tambahan';

    protected function define_columns($grid){       
       $grid->add('nama','Nama Kegiatan', true);
       $grid->add('tahun','Tahun', true);
       $grid->add('jenis','Jenis Tugas'); 
       $grid->add('target_kuantitas','Target Kuantitas', true);
       $grid->add('target_waktu','Target waktu', true);
       $grid->add('target_kualitas','Target Kualitas (%)', true);
       $grid->add('capaian_kuantitas','Capaian Kuantitas', true);
       $grid->add('capaian_kualitas','Capaian Kualitas (%)', true);
       $grid->add('status','Status'); 
	   $grid->edit('kinerja_tahunan/form', 'action','delete');
       
	   $grid->row(function ($row) {
//        if($row->data->jenis ==  1){$grid->edit('kinerja_tahunan/formtambahan', 'Edit','modify|delete');}else($grid->edit('kinerja_tahunan/form', 'Edit','modify|delete');)

        $row->cell('status')->value = $this->getStatusSign($row->data->status,$row->data->id);
        $row->cell('target_waktu')->value = $row->data->target_waktu." ".$row->data->nama_satuan_waktu;
        $new_link = '<a class="" title="Show" href="#" onclick=showDetail("/kinerja_tahunan/form?show=",'.$row->data->id.')><span class="glyphicon glyphicon-eye-open"> </span></a>';
		//$new_link2 = '<a class="" title="Edit" href="/kinerja_tahunan/form?update=",'.$row->data->id.')><span class="glyphicon glyphicon-eye-open"> </span></a>';
        $new_link = $new_link.'<a class="" title="edit" href="/kinerja_tahunan/form?modify='.$row->data->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
		if($row->data->jenis ==  1){
		  //$grid->edit('kinerja_tahunan/formtambahan', 'Edit','modify|delete');
          $row->cell('jenis')->value = "Tambahan";
          $row->cell('target_kuantitas')->value = "-";
          $row->cell('target_kualitas')->value = "-";
          $row->cell('capaian_kualitas')->value = "-";
          $row->cell('capaian_kuantitas')->value = "-";
          $new_link = '<a class="" title="Show" href="#" onclick=showDetail("/kinerja_tahunan/formtambahan?show=",'.$row->data->id.')><span class="glyphicon glyphicon-eye-open"> </span></a>';
		  //$new_link2 = '<a class="" title="Edit" href="/kinerja_tahunan/formtambahan?update=",'.$row->data->id.')><span class="glyphicon glyphicon-eye-open"> </span></a>';
          //$new_link2 = $new_link2.'<a class="text-danger" title="Delete" href="/kinerja_tahunan/formtambahan?delete='.$kinerja->id.'"><span class="glyphicon glyphicon-trash"> </span></a> ';
		   $new_link = $new_link.'<a class="" title="edit" href="/kinerja_tahunan/formtambahan?modify='.$row->data->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';

		} 
        else{
		//$grid->edit('kinerja_tahunan/form', 'Edit','modify|delete');	
          $row->cell('jenis')->value = "Jabatan";
          $row->cell('target_kuantitas')->value = $row->data->target_kuantitas." ".$row->data->nama_satuan_jumlah;
          $row->cell('capaian_kuantitas')->value = $row->data->capaian_kuantitas." ".$row->data->nama_satuan_jumlah;
        }
        $row->cell('_edit')->value = $new_link.'  '.$row->cell('_edit')->value;
       });
       //$grid->link('kinerja_tahunan/form', "Tambah Kegiatan" , "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','desc');
    }

    protected function define_fields($edit){   
      $user_id = $this->pegawai->getPegawaiId();   
      $tahun = Carbon::today()->format('Y');
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');
      
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
        $edit->set('tahun', $tahun);
        // Set satuan target waktu ke Bulan
        $edit->set('satuan_target_waktu', 2);
        $edit->set("posisi", $this->pegawai->getPosisiIdTerakhir());
        $edit->set('jenis', "0");
      }
      else{
        $edit->add('tahun','Tahun', 'text')->mode('readonly');
      }
      $edit->add('periode_awal','Periode Awal','select')->options(Util::get_all_bulan())->insertValue(1);
      $edit->add('periode_akhir','Periode Akhir','select')->options(Util::get_all_bulan())->insertValue(12);
      $edit->add('target_kuantitas','Target Kuantitas', 'text')->rule('required');
      $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());
      $edit->add('target_waktu','Target Waktu', 'text')->rule('required');
      $edit->add('target_kualitas','Target Kualitas (%)', 'text')->rule('required');

      if($edit->status == 'show'){
        $edit->text('capaian_kuantitas','Capaian Kuantitas')->mode('readonly');
        $edit->text('capaian_waktu','Capaian Waktu')->mode('readonly');
        $edit->text('capaian_kualitas','Capaian Kualitas (%)')->mode('readonly');
      }

      $edit->link("kinerja_tahunan",trans('rapyd::rapyd.back'))->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('kinerja_tahunan')->with('message', 'Kegiatan tahunan telah berhasil dimasukkan');
      });     
    }

    protected function define_fields_tambahan($edit){
      $user_id = $this->pegawai->getPegawaiId();
      $tahun = Carbon::today()->format('Y');
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');
      
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
        $edit->set('tahun', $tahun);
        // Set satuan target waktu ke Bulan
        $edit->set('satuan_target_waktu', 2);
        // Set target kuantitas dan satuannya
        $edit->set('target_kuantitas', 0);
        $edit->set('satuan_target_kuantitas', 0);
        $edit->set('target_kualitas', 0);

        $edit->set("posisi", $this->pegawai->getPosisiIdTerakhir());
        $edit->set('jenis', 1);
      }
      else{
        $edit->add('tahun','Tahun', 'text')->mode('readonly');
      }

      $edit->add('target_waktu','Target Waktu', 'text')->rule('required');

      $edit->link("kinerja_tahunan",trans('rapyd::rapyd.back'))->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('kinerja_tahunan')->with('message', 'Kegiatan tambahan tahunan telah berhasil dimasukkan');
      });
    }

    protected function define_filters($filter){
      $pegawai_id = $this->pegawai->getPegawaiId();
       $list_tahun = KinerjaTahunan::select('tahun')->where('pegawai_id', $pegawai_id)->distinct('tahun')->orderby('tahun', 'desc')->get();
       $array_opsi = array('' => 'Semua');
       foreach($list_tahun as $data_tahun){
          $array_opsi[$data_tahun->tahun] = $data_tahun->tahun;
       }
       $filter->add('nama','Nama', 'text')->scope( function ($query, $value) {
         if($value != null){
          return $query->where('kinerja_tahunan.nama', 'like', '%'.$value.'%');
         }
         return $query;
       });
       $filter->add('tahun','Tahun', 'select')->options($array_opsi);
       $filter->submit('search');
       $filter->reset('reset');
    }

    protected function get_dataset(){
        $user_id = $this->pegawai->getPegawaiId();   
        //$obj = new $this->model;
        return KinerjaTahunan::select("kinerja_tahunan.*", "satuan_jumlah.nama as nama_satuan_jumlah", "satuan_waktu.nama as nama_satuan_waktu")
              ->leftJoin("satuan_jumlah", "kinerja_tahunan.satuan_target_kuantitas", "=", "satuan_jumlah.id")
              ->leftJoin("satuan_waktu", "kinerja_tahunan.satuan_target_waktu", "=", "satuan_waktu.id")
              ->where("pegawai_id", $user_id)
              ->orderBy("jenis", "ASC");
        //return $obj::with('pegawai', 'satuan_target_kuantitas', 'satuan_target_waktu')->where('pegawai_id', $user_id);
    }

    protected function get_extra_context(){
        $extra_context = parent::get_extra_context();
        $tahun = Carbon::today()->format('Y');
        $extra_context['tahun'] = $tahun;
        return $extra_context;
    }
}
