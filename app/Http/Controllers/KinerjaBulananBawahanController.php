<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\SatuanJumlah;
use App\SatuanWaktu;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use App\Util;
use App\KomentarKinerjaBulanan;
use App\Notifikasi;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;

class KinerjaBulananBawahanController extends ModelController
{

  protected $model = '\\App\\KinerjaBulanan';

  protected $form_view = 'form_kinerja_bulanan';

  protected $page_title = 'Kinerja Bulanan';

  public function getIndex()
  {
    //$list_status = array("" => "Semua Status") + Util::getStatus();
    $list_pegawai = array();
    foreach($this->pegawai->getBawahan()->get() as $bawahan){
      $list_pegawai[$bawahan->id_pegawai] = $bawahan->nama;
    }
    //$list_pegawai = array("" => "Pilih Pegawai") + $list_pegawai;

    $list_bulan = Util::get_all_bulan();

    $list_tahun = KinerjaTahunan::select('tahun')->where('pegawai_id', $this->pegawai->getPegawaiId())->distinct('tahun')->orderby('tahun', 'desc')->lists('tahun', 'tahun');

    $selected_pegawai = "";
    if(Input::get('pegawai')){
      $selected_pegawai = Input::get('pegawai');        
    }

    $selected_bulan = Carbon::now()->month;
    if(Input::get('bulan')){
      $selected_bulan = Input::get('bulan');        
    }

    $selected_tahun = Carbon::now()->year;
    if(Input::get('tahun')){
      $selected_tahun = Input::get('bulan');        
    }

    $index_view = 'datatables/index_kegiatan_bulanan_bawahan';

    return view($index_view)
            ->with('list_pegawai',$list_pegawai)
            ->with('list_bulan',$list_bulan)
            ->with('list_tahun',$list_tahun)
            ->with('page_title',$this->page_title)
            ->with('selected_pegawai',$selected_pegawai)
            ->with('selected_bulan',$selected_bulan)
            ->with('selected_tahun',$selected_tahun)
            ->with('base_url',action(class_basename($this)."@getIndex"));
  }

  protected function define_fields($edit){       
    $tahun = Carbon::today()->format('Y');

    $pegawai_id = $edit->field('pegawai_id');
    
    $edit->add('nama','Nama Kegiatan', 'text')->rule('required');

    $edit->add('deskripsi','Deskripsi Kegiatan', 'redactor');

    $edit->add('bulan','Bulan','select')->options(Util::get_all_bulan());

    $edit->add('target_kuantitas','Target Kuantitas', 'text')->rule('required');

    $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());

    $edit->add('target_waktu','Target Waktu', 'text')->rule('required');

    $edit->add('satuan_target_waktu','<br/>','select')->options(SatuanWaktu::lists('nama', 'id')->all());

    $edit->add('target_kualitas','Target Kualitas (%)', 'text')->rule('required');

    $edit->add('kinerja_tahunan','Kegiatan Tahunan','select')->options(KinerjaTahunan::where('tahun', $tahun)->where("pegawai_id", $pegawai_id)->lists('nama', 'id'));
    
    if($edit->status == 'show'){
      $edit->text('capaian_kuantitas','Capaian Kuantitas')->mode('readonly');
      $edit->text('capaian_waktu','Capaian Waktu')->mode('readonly');
      $edit->text('capaian_kualitas','Capaian Kualitas (%)')->mode('readonly');
    }

  }

  public function getData(){
    $query = $this->get_dataset();
    $datatables = Datatables::of($query)
      ->addColumn('nama', function($kinerja) {
        return '<a href="#" onclick=showDetail("/kegiatan_bulanan_bawahan/form?show=",'.$kinerja->id.')> '.$kinerja->nama.' </a> ';
      })
      ->addColumn('bulan', function($kinerja) {
        return Util::get_bulan($kinerja->bulan);
      })
      ->addColumn('jenis', function($kinerja) {
        if($kinerja->jenis == 0){
          return "Jabatan";
        }
        if($kinerja->jenis == 1){
          return "Tambahan";
        }
        return "";
      })
      ->addColumn('capaian_kuantitas', function($kinerja) {
        if($kinerja->capaian_kuantitas == 0){
          return 0;
        }
        return $kinerja->capaian_kuantitas/$kinerja->target_kuantitas*100;
      })
      ->addColumn('status', function($kinerja) {
        return $this->getStatusSign($kinerja->status,$kinerja->id);
      })
      ->addColumn('action', function($kinerja) {
            $new_link = '';
            $new_link = '<a class="btn btn-info btn-sm" title="Show" href="#" onclick=showDetail("/kinerja_harian_bawahan/timelineharian?kinerja_bulanan=",'.$kinerja->id.')> Harian </a> '.$new_link;
            $new_link = $new_link.'<a class="btn btn-success btn-sm" title="Accept" href="#" onclick=accept("/kegiatan_bulanan_bawahan/accept",'.$kinerja->id.')> Setuju </a> ';
            $new_link = $new_link.'<a class="btn btn-danger btn-sm" title="Reject" href="#" onclick=reject("/kegiatan_bulanan_bawahan/reject",'.$kinerja->id.')> Tolak </a> ';
            return $new_link;
          }
      );

      if($pegawai = $datatables->request->get('pegawai')) {
        $datatables->where('kinerja_bulanan.pegawai_id', $pegawai);
      }

      if($bulan = $datatables->request->get('bulan')) {
        $datatables->where('kinerja_bulanan.bulan', $bulan);
      }

      if($tahun = $datatables->request->get('tahun')) {
        $datatables->where('kinerja_bulanan.tahun', $tahun);
      }

      return $datatables->make(true);
  }

  public function anyAccept(Request $request){
    $edit = \DataEdit::source(new KinerjaBulanan);
    $edit->add('status','Status Kegiatan', 'hidden');
    $edit->add('target_kuantitas','Target Kuantitas', 'text')->mode('readonly');
    $edit->add('satuan_target_kuantitas','Satuan Target Kuantitas', 'select')->options(SatuanJumlah::lists('nama', 'id')->all())->mode('readonly');
    $edit->add('capaian_kualitas','Capaian Kualitas', 'text');
    $extra_context = array("action" => "accept");
    return $this->changestatus($request, $edit, $extra_context, Util::STATUS_SETUJU);
  }

  public function anyReject(Request $request){
    $edit = \DataEdit::source(new KinerjaBulanan);
    $edit->add('status','Status Kegiatan', 'hidden');
    $extra_context = array("action" => "reject");
    return $this->changestatus($request, $edit, $extra_context, Util::STATUS_TOLAK);
  }

  protected function changeStatus(Request $request, $edit, $extra_context, $status){
    $edit->saved(function () use ($edit, $request) {
      // Hitung capaian berdasarkan status harian
      $edit->model->changeStatus($edit->model->status, $edit->model->capaian_kualitas);
      
      // Hitung ulang notifikasi sebagai atasan
      $this->pegawai->hitungNotifikasiSebagaiAtasan();
      // Hitung ulang notifikasi pegawai
      Notifikasi::hitungNotifikasiSebagaiPegawai($edit->model->pegawai_id);

      // Insert komentar baru
      $now = Carbon::now('Asia/Jakarta');
      KomentarKinerjaBulanan::insert([
          'status' => $edit->model->status,
          'kinerja_bulanan_id' => $edit->model->id,
          'komentar' => $request->get('komentar'),
          'created_at' => $now
        ]);
    }); 
    
    $edit->build();
    // If untuk menampilkan form
    if($edit->action == 'idle'){    
      $form_view = 'ajax.form_konfirmasi_ubah_status';
      if($status ==  Util::STATUS_SETUJU){
        $form_view = "ajax.form_konfirmasi_bulanan_accept";
      }
    }
    // Else, kerjakan action dan cek statusnya
    else{
      if($edit->process_status == 'success'){
        return response()->json(array("status"=>$edit->process_status));
      }
      else{
        return response()->json(array("status"=>$edit->process_status),400);
      }
    }      
    $result = $edit->view($form_view, compact('edit'))->with($extra_context);     
    return $result;
  }
  
  protected function get_dataset(){
    return KinerjaBulanan::with('satuan_target_kuantitas', 'satuan_target_waktu', 'kinerja_tahunan');
  }
  
}
