<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Response;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Zofe\Rapyd\DataGrid;
use Zofe\Rapyd\DataFilter;
use Zofe\Rapyd\DataEdit;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use DB;

use App\Util;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $pagination = 20;

    protected $model = '';

    protected $index_view = null;

    protected $form_view = null;

    protected $page_title = '';

    protected $extra_context = array();

    protected $request = null;

    protected $pegawai = null;

    public function __construct()
    {
        $this->middleware('auth');
        if(!Auth::user()){
          if (Request::ajax()) {
              return response('Halaman tidak berhak diakses.', 401);
          } else {
              return redirect()->guest('auth/login')->send();
          }
        }
        $this->pegawai = Util::getPegawai();
        View::share('pegawai_global', $this->pegawai);
    }

    public function getIndex()
    {
        // Get dataset
        $query = $this->get_dataset();
        // Define Data Filter
        $filter = \DataFilter::source($query);
        $this->define_filters($filter);
        // Define dataset and columns
        $grid = \DataGrid::source($filter);
        $grid->paginate($this->pagination);
        $this->define_columns($grid);
        $this->define_ordering($grid);
        // Define filters
        $extra_context = $this->get_extra_context();
        if(Request::ajax()){
            $grid->build();
            $json_arr = array();        
            foreach($grid->getData() as $row){
                array_push($json_arr, $row);
            }
            return response()->json($json_arr);
        }

        $index_view = (isset($this->index_view) ? 
            $this->index_view : 'index');

       return view($index_view, compact('grid', 'filter'))
                ->with('page_title',$this->page_title)
                ->with('base_url',action(class_basename($this)."@getIndex"))
                ->with($extra_context);
    }

    public function anyForm(Request $request){
        $edit = \DataEdit::source(new $this->model);

        if(!$this->checkFormPermission($edit)){
            if(Request::ajax()){
                return response('Halaman tidak berhak diakses.', 401);
            }
            else{
                return Redirect::to($this->getRedirectUrl())->with('message', 'Halaman tidak berhak diakses');
            }
        }

        $page_activity = 'Tambah';
        if($edit->status == 'modify'){
            $page_activity = 'Ubah';
        }
        else if($edit->status == 'show'){
            $page_activity = 'Detail';
        }

        $form_view = (isset($this->form_view) ? 
            $this->form_view : 'form');
        $this->define_fields($edit);
        $extra_context = $this->get_extra_context();
        
        if(Request::ajax()){
            $edit->build();
            // If untuk menampilkan form
            if($edit->action == 'idle'){              
              $content_type = Request::header('Accept');
              switch ($edit->status) {
                  case 'create':
                  case 'modify' :
                  case 'delete':
                  case 'show':
                      if($content_type == 'application/json'){
                          $data_array = array();
                          foreach ($edit->fields as $field) {
                              $data_array[$field->name] = $field->value;
                          }
                          return response()->json($data_array);
                      }
                      else{
                          $form_view = 'ajax.'.$this->form_view;
                      }
                      break;
                  default:
                      return response()->json(array("err" => "unknown"), 404);
              }           
            }
            // Else, kerjakan action dan cek statusnya
            else{
              if($edit->process_status == 'success'){
                return response()->json(array("status"=>$edit->process_status));
              }
              else{
                return response()->json(array("status"=>$edit->process_status),400);
              }
            }
        }

        $result = $edit->view($form_view,compact('edit'))
              ->with('page_title',$this->page_title)
              ->with('page_activity',$page_activity)
              ->with('base_url',action(class_basename($this)."@getIndex"))
              ->with($extra_context);     
        return $result;
    }

    protected function define_columns($grid){
           
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','desc');
    }

    protected function define_filters($filter){

    }

    protected function define_fields($edit){

    }

    protected function get_dataset(){
        $obj = new $this->model;
        return $obj;
    }

    protected function get_extra_context(){
        return $this->extra_context;
    }

    protected function getStatusSign($status, $id){
      if($status == Util::STATUS_SETUJU){
        $sign = "<i class='fa fa-check' id=".$id." />";
      }
      else if($status == Util::STATUS_TOLAK){
        $sign = "<i class='fa fa-times' id=".$id." />";
      } 
      else{
        $sign = "<i class='fa fa-question' id=".$id." />";
      } 
      return $sign;
    }

    protected function checkFormPermission($edit){
        return true;
    }

    protected function getRedirectUrl(){
      return '/';
    }
}
