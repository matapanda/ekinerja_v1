<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Req;

use Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class KinerjaController extends ModelController
{
    protected $page_title_tambahan = '';

    protected $form_view_tambahan = '';

    protected function define_fields_tambahan($edit){
        
    }

    public function anyFormtambahan(Request $request){
        $edit = \DataEdit::source(new $this->model);

        if(!$this->checkFormPermission($edit)){
            if(Request::ajax()){
                return response('Halaman tidak berhak diakses.', 401);
            }
            else{
                return Redirect::to($this->getRedirectUrl())->with('message', 'Halaman tidak berhak diakses');
            }
        }

        $page_activity = 'Tambah';
        if($edit->status == 'modify'){
            $page_activity = 'Ubah';
        }
        else if($edit->status == 'show'){
            $page_activity = 'Detail';
        }

        $form_view = $this->form_view_tambahan;
        $this->define_fields_tambahan($edit);
        $extra_context = $this->get_extra_context();
        
        if(Request::ajax()){
            $edit->build();
            // If untuk menampilkan form
            if($edit->action == 'idle'){              
              $content_type = Request::header('Accept');
              switch ($edit->status) {
                  case 'create':
                  case 'modify' :
                  case 'delete':
                  case 'show':
                      if($content_type == 'application/json'){
                          $data_array = array();
                          foreach ($edit->fields as $field) {
                              $data_array[$field->name] = $field->value;
                          }
                          return response()->json($data_array);
                      }
                      else{
                          $form_view = 'ajax.'.$this->form_view_tambahan;
                      }
                      break;
                  default:
                      return response()->json(array("err" => "unknown"), 404);
              }           
            }
            // Else, kerjakan action dan cek statusnya
            else{
              if($edit->process_status == 'success'){
                return response()->json(array("status"=>$edit->process_status));
              }
              else{
                return response()->json(array("status"=>$edit->process_status),400);
              }
            }
        }

        $result = $edit->view($form_view,compact('edit'))
              ->with('page_title', $this->page_title_tambahan)
              ->with('page_activity', $this->page_title_tambahan)
              ->with('base_url',action(class_basename($this)."@getIndex"))
              ->with($extra_context);     
        return $result;
    }
    
}
