<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\GajiGolongan;
use App\GolRuang;

class GajiGolonganController extends ModelController
{
    protected $model = '\\App\\gajigolongan';

    protected $page_title = 'Nominal Gaji Berdasarkan Golongan Ruang';

    protected $form_view = 'form';

    protected function define_columns($grid){
        $grid->add('{{ $keterangan_golruang }}','Golongan Ruang', true);
        $grid->add('nominal','Nominal', true);
        $grid->edit('gajigolongan/form', 'Edit','');
        $grid->row(function ($row) {
            $row->cell('_edit')->value = '<a class="" title="Edit" href="/gajigolongan/form?id='.$row->data->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
        });
    }

    public function anyForm(Request $request){  
        if(Input::has('id')){
            $id = Input::get("id"); 
        }
        else{
            return Redirect::to('gajigolongan')->with('message' , 'Halaman tidak bisa diakses');
        }     
        $gaji_golongan = GajiGolongan::where('id' , $id)->first();
        $golruang = GolRuang::where('id_golruang', $gaji_golongan->id_golruang)->first();
        $edit = \DataForm::source($gaji_golongan);
        $edit->add('id_golruang', 'Golongan Ruang','select')->options(array($golruang->id_golruang => $golruang->keterangan))->mode('readonly');
        $edit->add('nominal', 'Nominal','text');
        $edit->link('gajigologan',trans('rapyd::rapyd.back'))->back();
        $edit->saved(function () use ($edit) {        
            return Redirect::to('gajigolongan')->with('message' , 'Data atasan berhasil disimpan');
        }); 
        $page_activity = 'Ubah';
        $edit->submit('Simpan','BR');
        $result = $edit->view($this->form_view,compact('edit'))
              ->with('page_title',$this->page_title)
              ->with('page_activity',$page_activity)
              ->with('base_url',action(class_basename($this)."@getIndex"));     
        return $result;
    }


     protected function get_dataset(){
        $dataset = GajiGolongan::select("id","golruang.keterangan as keterangan_golruang", "nominal")
                        ->leftJoin("golruang", "gaji_golongans.id_golruang", "=", "golruang.id_golruang");
        return $dataset;
    }

}
