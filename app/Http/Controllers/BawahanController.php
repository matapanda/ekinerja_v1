<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Instansi;
use App\Jabatan;
use App\Pegawai;
use App\KinerjaHarian;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use Zofe\Rapyd\DataGrid;
use App\Util;
use App\IdentitasPegawai;

class BawahanController extends ModelController
{

    protected $pegawai = null;

    protected $model = '\\App\\Pegawai';

    public function __construct()
    {
        parent::__construct();
    }

    protected $page_title = 'Pegawai Bawahan';

    protected $form_view = 'bawahan_profile';

    protected function define_columns($grid){
       $grid->add('nip_baru','NIP', true);
       $grid->add('nama','Nama', true); 
       $grid->row(function ($row) {
         $new_link = '<a class="" title="Detail" href="bawahan/show/'.$row->data->id_pegawai.'">'.$row->data->nama.'</a>';
         $row->cell('nama')->value = $new_link;
       });
       //$grid->add('keterangan_posisi','Jabatan'); 
       //$grid->add('keterangan_unitk','Instansi');
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama', 'text')->rule('required|max:255');
      $edit->add('nip','NIP', 'text')->rule('required|max:20');
      //$edit->add('email','Email', 'text')->rule('required|email|max:255');
      //$instansi_arr = Instansi::getNestedList('nama', null, '---');
      //$edit->add('instansi','Instansi','select')->options($instansi_arr);
      //$edit->add('jabatan','Jabatan','select')->options(Jabatan::lists('nama', 'id')->all());

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('bawahan')->with('message', 'Success');
      });     
    }

    public function getShow(Request $request, $pegawai_id){
      $pegawai = Util::getPegawaiWithPosisi($pegawai_id);
      if(!$pegawai){
        return Redirect::to('bawahan')->with('message', 'Pegawai tidak ditemukan');
      }    
        
      $list_tahun = KinerjaTahunan::select('tahun')->where('pegawai_id', $pegawai_id)->distinct('tahun')->orderby('tahun', 'desc')->lists('tahun', 'tahun');

      $active_tab = 'harian';
      if($request->input('activetab')){
        if($request->input('activetab')=='tahunan'){
          $active_tab = 'tahunan'; 
        }        
      }

      $extra_context = $this->get_extra_context();
      return View::make($this->form_view)
              ->with('pegawai' , $pegawai->first())
              ->with('list_tahun', $list_tahun)
              ->with('page_title',$this->page_title)
              ->with('base_url',action(class_basename($this)."@getIndex"))
              ->with($extra_context);
    
    }    

    protected function get_dataset(){
        return $this->pegawai->getBawahan();     
    }

    public function getTest(Request $request, $id){
      return $request->input('file');
    }

    protected function define_ordering($grid){
        //$grid->orderBy('id','desc');
    }
}
