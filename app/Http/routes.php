<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'DashboardController@getIndex')->middleware('auth');
Route::controller('/dashboard', 'DashboardController');
Route::controller('/jabatan', 'JabatanController');
Route::controller('/instansi', 'InstansiNewController');
Route::controller('/satuan_jumlah', 'SatuanJumlahController');
Route::controller('/satuan_waktu', 'SatuanWaktuController');
Route::controller('/pegawai', 'PegawaiController');
//Route::controller('/kinerja_tahunan_bulanan', 'KinerjaTahunanBulananController');
Route::controller('/kinerja_tahunan', 'KinerjaTahunanController');
Route::controller('/kinerja_bulanan', 'KinerjaBulananController');

Route::controller('/kinerja_harian_bawahan', 'KinerjaHarianBawahanController');
Route::controller('/kegiatan_bulanan_bawahan', 'KinerjaBulananBawahanController');
Route::controller('/kinerja_tahunan_bawahan', 'KinerjaTahunanBawahanController');
Route::controller('/bawahan', 'BawahanController');
Route::controller('/peg_skpd', 'PegawaiSKPDController');
Route::controller('/keg_tahunan_bulanan_peg_skpd', 'KinerjaTahunanBulananPegawaiSKPDController');
Route::controller('/keg_harian_peg_skpd', 'KinerjaHarianPegawaiSKPDController');

Route::controller('/auth', 'Auth\AuthController');

Route::controller('/kinerja_harian', 'KinerjaHarianController');

Route::controller('/notifikasi', 'NotifikasiController');

Route::controller('/skp', 'NilaiSKPBulananController');

Route::controller('/admin_nilai_skp_bulanan', 'AdminNilaiSKPBulananController');

Route::controller('/test', 'TestController');

Route::controller('/atasanbawahan', 'AtasanBawahanController');

//Route::controller('/gajigolongan', 'GajiGolonganController');

Route::controller('/report', 'ReportController');

Route::get('/getReport', 'ReportController@getLaporan');

Route::get('/tunjangan', 'ReportController@getTunjangan');

Route::controller('/bobot_jabatan', 'BobotJabatanController');

Route::controller('/klasifikasi_bobot_jabatan', 'KlasifikasiBobotJabatanController');

Route::any('/setting', 'SettingController@anyForm');

Route::controller('/user', 'UserController');

Route::controller('/kinerja_tahunan_bulanan_bawahan', 'KinerjaTahunanBulananBawahanController');

Route::any('/pilih_atasan', 'PilihAtasanBawahanController@anyForm');

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ["namespace" => "App\Http\Controllers"], function ($api) {
    $api->get('/', function() {
        return ['Fruits' => 'Delicious and healthy!'];
    });
    $api->get('/tahunan', function() {
        return response()->json(\App\KinerjaTahunan::all());
    });
    $api->post('authenticate', 'ApiAuthController@authenticate');
    $api->post('logout', 'ApiAuthController@logout');
    $api->get('token', 'ApiAuthController@getToken');
    $api->get('profile', 'ApiAuthController@getProfile');
});

Route::post('oauth/access_token', function() {
    return response()->json(Authorizer::issueAccessToken());
});

Route::get('oauth/profile', function() {
    return response()->json(Auth::user());
});



